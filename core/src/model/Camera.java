package model;

public class Camera {
    private float cameraX = 0f;
    private float cameraY = 0f;
    private static boolean cameraState = true;

	public void update(float newCamX, float newCamY) {
        this.cameraX = newCamX;
        this.cameraY = newCamY;
    }

    public void setCameraX(float cameraX) {
		this.cameraX = cameraX;
	}

	public void setCameraY(float cameraY) {
		this.cameraY = cameraY;
	}

	public float getCameraX() {
        return cameraX;
    }

    public float getCameraY() {
        return cameraY;
    }
    
    public static boolean isCameraState() {
  		return cameraState;
  	}

  	public static void setCameraState(boolean cameraState) {
  		Camera.cameraState = cameraState;
  	}
}
