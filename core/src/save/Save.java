package save;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import model.Actor;

public class Save {
	
	public static ArrayList<Integer> saveFileInfo = new ArrayList<Integer>();
	
	public static void saveGame(){
	    try {
	      File Save = new File("files/Save/Save1.txt");
	      if (Save.createNewFile()) {
	        System.out.println("Fichier de sauvegarde cr�e : " + Save.getName());
	      } else {
	        System.out.println("Fichier de sauvegarde d�j� existant.");
	      }
	      
	      FileWriter saveUpdate = new FileWriter("files/Save/Save1.txt");
	      saveUpdate.write(/*"PlayerX : "*/screen.GameScreen.getPlayer().getX()+"\n");
	      saveUpdate.write(/*"PlayerY : "+*/screen.GameScreen.getPlayer().getY()+"\n");
	      //saveUpdate.write("Player$ : "+/*Main.argent+*/"\n");  //Save player Money
	      //saveUpdate.write("PlayerXP : "+/*Main.experience+*/"\n");  //Save player XP and level
	      //saveUpdate.write("PlayerLVL : "/*Main.niveau*/+"\n");
	      //saveUpdate.write("PlayerMap : "+/*StaticMethod.playerMap+*/"\n");  //Saves the map where the player is on
	      
	      
	      saveUpdate.close();
	      System.out.println("Partie sauvegard�e");
	    } catch (IOException e) {
	      System.out.println("Erreur");
	      e.printStackTrace();
	    }    
	    
	  }

	
	public static void loadSave() /*throws Exception*/ {
		
		try{
			File Save = new File("files/Save/Save1.txt");
			InputStream ips=new FileInputStream(Save); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne = null;
			while ((ligne=br.readLine())!=null){
				//System.out.println(ligne);
				saveFileInfo.add(Integer.parseInt(ligne));
			}

			screen.GameScreen.setPlayer(saveFileInfo.get(0),saveFileInfo.get(1));
			System.out.println("Sauvegarde charg�");
			
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
	}
	
}
