# -*-coding:Utf-8 -*

from tkinter import *
from PIL import Image, ImageFont, ImageTk
from enum import *
import gc
import time


def min(a, b):
    """returns the value for a that can't be lower than b
    used to force a variable not being too low"""
    if a<b:
        return b
    return a

def max(a, b):
    """returns the value for a that can't be higher than b
    used to force a variable not being too high"""
    if a>b:
        return b
    return a

def stringToBool(s, err=False):
    """converts the string to a boolean. if "err" is True, raise an error if it is not a boolean. return False otherwhise.
    correct booleans are
    -True: "t", "true", "True", "y", "yes"
    -False: "f", "false", False, "n", "no" """
    isBoolean=False
    s=s.strip()
    if s=="t" or s=="true" or s=="True" or s=="y" or s=="yes":
        isBoolean=True
    else:
        if err==True:
            if s=="f" or s=="false" or s=="False" or s=="n" or s=="no":
                isBoolean=False#yeah it's useless
            else:
                raise Exception("'{}' is not a boolean".format(s))
    return isBoolean

class mode(Enum):
    colision=1
    sol=2
    texture=3
    over=4
    texture3eC=5
    entites=6

class Tile:
    def __init__(self, x, y):
        self.x=x
        self.y=y
        self.colision=False
        self.sol=0
        self.texture=0
        self.texture3eC=0

        self.over=False

        self.alreadyChanged=False

        #self.displayedColision=ImageTk.PhotoImage(Image.open("imgs/cross.png"))
        #self.displayedOver=ImageTk.PhotoImage(Image.open("imgs/cross.png"))

        self.displayedGround=None
        self.groundid=-1
        self.displayedTexture=None
        self.textureid=-1
        self.displayedColision=None
        self.colisionid=-1
        self.displayedOver=None
        self.overid=-1
        self.displayedTexture3eC=None
        self.texture3eCid=-1
        self.entiteid=-1
        self.displayedEntite=None

        self.entite=None

    def __str__(self):
        return str("("+str(self.x)+", "+str(self.y)+")")

    def isInScreen(self):
        global m
        inScreen=False
        xTileBegin=self.x*m.tileSize
        xTileEnd=xTileBegin+m.tileSize
        xScreenBegin=m.canvMap.canvasx(0)
        #xScreenEnd=xScreenBegin+m.canvMap.size[0]
        #m.canvMap.winfo_width
        xScreenEnd=xScreenBegin+m.canvMap.winfo_width()
        #print("screen x size: {}".format(m.canvMap.winfo_width()))
        if xTileBegin<xScreenEnd and xTileEnd>xScreenBegin:
            yTileBegin=self.y*m.tileSize
            yTileEnd=yTileBegin+m.tileSize
            yScreenBegin=m.canvMap.canvasy(0)
            #yScreenEnd=yScreenBegin+m.canvMap.size[1]
            yScreenEnd=yScreenBegin+m.canvMap.winfo_height()
            #print("screen y size: {}".format(m.canvMap.winfo_height()))
            if yTileBegin<yScreenEnd and yTileEnd>yScreenBegin:
                inScreen=True
        return inScreen

    def show(self):
        global m
        if self.isInScreen():
            """if self.groundid!=-1:
                m.canvMap.delete(self.groundid)
            if self.textureid!=-1:
                m.canvMap.delete(self.textureid)
            if self.colisionid!=-1:
                m.canvMap.delete(self.colisionid)
            if self.overid!=-1:
                m.canvMap.delete(self.overid)"""
            self.hide()
            """self.groundid=-1
            self.textureid=-1
            self.colisionid=-1
            self.overid=-1"""
            self.refreshTextures()
            if m.mode==mode.colision:
                self.showColisionMode()
            if m.mode==mode.texture:
                self.showTextureMode()
            if m.mode==mode.sol:
                self.showGroundMode()
            if m.mode==mode.over:
                self.showOverMode()
            if m.mode==mode.texture3eC:
                self.showTexture3eCMode()
            if m.mode==mode.entites:
                self.showEntiteMode()
        else:
            self.hide()

    def hide(self):
        if self.groundid!=-1:
            m.canvMap.delete(self.groundid)
        if self.textureid!=-1:
            m.canvMap.delete(self.textureid)
        if self.colisionid!=-1:
            m.canvMap.delete(self.colisionid)
        if self.overid!=-1:
            m.canvMap.delete(self.overid)
        if self.texture3eCid!=-1:
            m.canvMap.delete(self.displayedTexture3eC)
        if self.entiteid!=-1:
            m.canvMap.delete(self.entiteid)
        self.groundid=-1
        self.textureid=-1
        self.colisionid=-1
        self.overid=-1
        self.texture3eCid=-1
        self.entiteid=-1
        self.displayedGround=None
        self.displayedTexture=None
        self.displayedColision=None
        self.displayedOver=None
        self.displayedTexture3eC=None
        self.displayedEntite=None

    def showGroundTexture(self):
        global m
        self.groundid=m.canvMap.create_image(self.x*m.tileSize, self.y*m.tileSize, image=self.displayedGround, anchor=NW)

    def showTexture(self):
        global m
        self.textureid=m.canvMap.create_image(self.x*m.tileSize, self.y*m.tileSize, image=self.displayedTexture, anchor=NW)

    def showTexture3eC(self):
        global m
        self.texture3eCid=m.canvMap.create_image(self.x*m.tileSize, self.y*m.tileSize, image=self.displayedTexture3eC, anchor=NW)

    def showColision(self):
        global m
        self.colisionid=m.canvMap.create_image(self.x*m.tileSize, self.y*m.tileSize, image=self.displayedColision, anchor=NW)

    def showOver(self):
        global m
        self.overid=m.canvMap.create_image(self.x*m.tileSize, self.y*m.tileSize, image=self.displayedOver, anchor=NW)

    def showEntite(self):
        global m
        if self.entite!=None:
            self.entiteid=m.canvMap.create_image(self.x*m.tileSize, self.y*m.tileSize, image=self.displayedEntite, anchor=NW)

    def showAllTextures(self):
        self.showGroundTexture()
        self.showTexture()
        self.showTexture3eC()

    #---differents modes---
    def showColisionMode(self):
        global m
        self.showAllTextures()
        #self.colisionid=m.canvMap.create_image(self.x*m.tileSize, self.y*m.tileSize, image=self.displayedColision, anchor=NW)
        self.showColision()

    def showGroundMode(self):
        global m
        self.showGroundTexture()

    def showTextureMode(self):
        global m
        #self.groundid=m.canvMap.create_image(self.x*m.tileSize, self.y*m.tileSize, image=self.displayedGround, anchor=NW)
        #self.textureid=m.canvMap.create_image(self.x*m.tileSize, self.y*m.tileSize, image=self.displayedTexture, anchor=NW)
        self.showGroundTexture()
        self.showTexture()

    def showOverMode(self):
        global m
        self.showAllTextures()
        #self.overid=m.canvMap.create_image(self.x*m.tileSize, self.y*m.tileSize, image=self.displayedOver, anchor=NW)
        self.showOver()

    def showTexture3eCMode(self):
        self.showAllTextures()

    def showEntiteMode(self):
        self.showAllTextures()
        self.showEntite()

    def changeTexture(self, x, y, refresh=False):
        global m
        #print("{}, {}".format(x, y))
        self.texture=x+y*m.textureTileset.size[0]//m.tileSize
        if refresh:
            x=x*m.tileSize
            y=y*m.tileSize
            self.displayedTexture=ImageTk.PhotoImage(m.textureTileset.crop((x, y, x+m.tileSize, y+m.tileSize)))
            self.show()
            self.alreadyChanged=True
        #if self.texture!=0:
            #print("texture: "+str(self.texture))

    def changeTexture3eC(self, x, y, refresh=False):
        global m
        #print("{}, {}".format(x, y))
        self.texture3eC=x+y*m.textureTileset.size[0]//m.tileSize
        if refresh:
            x=x*m.tileSize
            y=y*m.tileSize
            self.displayedTexture3eC=ImageTk.PhotoImage(m.textureTileset.crop((x, y, x+m.tileSize, y+m.tileSize)))
            self.show()
            self.alreadyChanged=True
        #if self.texture!=0:
            #print("texture: "+str(self.texture))

    def changeSol(self, x, y, refresh=False):
        global m
        self.sol=x+y*m.groundTileset.size[0]//m.tileSize
        if refresh:
            x=x*m.tileSize
            y=y*m.tileSize
            #print(m.groundTileset)
            self.displayedGround=ImageTk.PhotoImage(m.groundTileset.crop((x, y, x+m.tileSize, y+m.tileSize)))
            self.show()
            self.alreadyChanged=True

    def changeColision(self, col, refresh=False):
        self.colision=col
        #print("changed colision")
        if refresh:
            if col:
                img=Image.open("imgs/cross.png")
            else:
                img=Image.open("imgs/v.png")
            self.displayedColision=ImageTk.PhotoImage(img)
            self.show()
            self.alreadyChanged=True

    def changeOver(self, over, refresh=False):
        self.over=over
        if refresh:
            if over:
                img=Image.open("imgs/v.png")
            else:
                img=Image.open("imgs/cross.png")
            self.displayedOver=ImageTk.PhotoImage(img)
            self.show()
            self.alreadyChanged=True

    def changeEntite(self, ent, refresh=False):
        self.entite=ent
        if refresh:
            self.refreshEntite()
            self.show()

    def refreshTextures(self):
        self.refreshGround()
        self.refreshNormalTexture()
        self.refreshColision()
        self.refreshOver()
        self.refreshTexture3eC()
        self.refreshEntite()

    def refreshGround(self):
        x=self.sol%(m.groundTileset.size[0]//m.tileSize)
        y=self.sol//(m.groundTileset.size[0]//m.tileSize)
        x=x*m.tileSize
        y=y*m.tileSize
        self.displayedGround=ImageTk.PhotoImage(m.groundTileset.crop((x, y, x+m.tileSize, y+m.tileSize)))


    def refreshNormalTexture(self):
        x=self.texture%(m.textureTileset.size[0]//m.tileSize)
        y=self.texture//(m.textureTileset.size[0]//m.tileSize)
        x=x*m.tileSize
        y=y*m.tileSize
        #print("refresh: {}, {}".format(x, y))
        self.displayedTexture=ImageTk.PhotoImage(m.textureTileset.crop((x, y, x+m.tileSize, y+m.tileSize)))

    def refreshTexture3eC(self):
        x=self.texture3eC%(m.textureTileset.size[0]//m.tileSize)
        y=self.texture3eC//(m.textureTileset.size[0]//m.tileSize)
        x=x*m.tileSize
        y=y*m.tileSize
        #print("refresh: {}, {}".format(x, y))
        self.displayedTexture3eC=ImageTk.PhotoImage(m.textureTileset.crop((x, y, x+m.tileSize, y+m.tileSize)))


    def refreshColision(self):
        if self.colision:
            #img=Image.open("imgs/cross.png")
            img=ImageTk.PhotoImage(m.crossImage.crop((0, 0, m.tileSize, m.tileSize)))
        else:
            #img=Image.open("imgs/v.png")
            img=ImageTk.PhotoImage(m.vImage.crop((0, 0, m.tileSize, m.tileSize)))
        #self.displayedColision=ImageTk.PhotoImage(img)
        self.displayedColision=img


    def refreshOver(self):
        if self.over:
            #img=Image.open("imgs/v.png")
            img=ImageTk.PhotoImage(m.vImage.crop((0, 0, m.tileSize, m.tileSize)))
        else:
            #img=Image.open("imgs/cross.png")
            img=ImageTk.PhotoImage(m.crossImage.crop((0, 0, m.tileSize, m.tileSize)))
        #self.displayedOver=ImageTk.PhotoImage(img)
        self.displayedOver=img

    def refreshEntite(self):
        global m
        if self.entite==None:
            self.displayedEntite=None
        else:
            try:
                path="../"+self.entite.spritePath
                self.displayedEntite=ImageTk.PhotoImage(Image.open(path))
            except Exception as e:
                print("erreur: "+str(e))
                self.displayedEntite=ImageTk.PhotoImage(m.crossImage.crop((0, 0, m.tileSize, m.tileSize)))

class Entite:
    def __init__(self, posx, posy, actions, sPath, traverse=False):
        self.act=actions
        self.posx=posx
        self.posy=posy
        self.spritePath=sPath
        self.traverse=False

    def getInfos(self):
        infos=str(self.posx)+"; "+str(self.posy)+"; "+self.spritePath+"; "+str(self.traverse)+"\n"
        infos+=self.act
        return infos



class Main:
    def __init__(self):
        ##-----Vatiables d'information
        self.map=[]
        self.entites=[]
        self.tileSize=32
        self.mapXSize=32
        self.mapYSize=18
        self.mapId=1
        self.selectId=-1
        self.selectEndId=-1

        self.changeMapWindow=None
        self.optionsWindow=None
        self.entityModifier=None

        self.createMap()

        self.choicex=0
        self.choicey=0
        self.choicexEnd=0
        self.choiceyEnd=0

        self.firstClickMap=True
        self.firstClickX=0
        self.firstClickY=0

        self.mode=mode.colision

        self.mapPath=("../files/maps/")

        self.tilesetPath=("../assets/graphics/tilesets/")
        self.groundTilesetName=("Outside_A5.png")
        self.textureTilesetName=("Outside_B.png")

        self.vImage=Image.open("imgs/v.png")
        self.crossImage=Image.open("imgs/cross.png")
        self.voidImage=Image.open("imgs/vide.png")

        self.groundTileset=None
        self.displayedGroundTileset=None
        self.textureTileset=None
        self.displayedTileset=None
        #self.select=ImageTk.PhotoImage(Image.open("imgs/v.png"))
        self.select=None
        self.selectEnd=None

        ##-----Creation de la fenetre-----##
        self.editeur=Tk()
        self.editeur.title("éditeur")
        self.editeur.resizable(0, 0)
        self.editeur.protocol("WM_DELETE_WINDOW", self.quitter)

        ##-----Creation des boutons-----##
        #  boutons choix du mode
        modeRow=1
        self.bouton_colisions = Button(self.editeur, text='colisions', command=self.modeToColision)
        self.bouton_colisions.grid(row = modeRow, column = 0, pady=5)

        self.bouton_sol = Button(self.editeur, text='textures du sol', command=self.modeToSol)
        self.bouton_sol.grid(row = modeRow, column = 1, pady=5)

        self.bouton_textures = Button(self.editeur, text='textures de la 2e couche', command=self.modeToTexture)
        self.bouton_textures.grid(row = modeRow, column = 2, pady=5)

        self.bouton_textures3eC = Button(self.editeur, text='textures de la 3e couche', command=self.modeToTexture3eC)
        self.bouton_textures3eC.grid(row = modeRow, column = 3, pady=5)

        self.bouton_over = Button(self.editeur, text='tiles au dessus', command=self.modeToOver)
        self.bouton_over.grid(row = modeRow, column = 4, pady=5)

        self.bouton_ent = Button(self.editeur, text='entites', command=self.modeToEntity)
        self.bouton_ent.grid(row = modeRow, column = 5, pady=5)


        #  boutons options
        choiceRow=6
        self.bouton_changeMap = Button(self.editeur, text='changer de map', command=self.changeMap)
        self.bouton_changeMap.grid(row = choiceRow, column = 0, pady=5)

        self.bouton_optionsMap = Button(self.editeur, text='options de la map', command=self.optionsMap)
        self.bouton_optionsMap.grid(row = choiceRow, column = 4, pady=5)


        #  boutons enregistrer et quitter
        optionsRow=7
        #self.bouton_quitter = Button(self.editeur, text='Quitter', command=self.editeur.destroy)
        self.bouton_quitter = Button(self.editeur, text='Quitter', command=self.quitter)
        self.bouton_quitter.grid(row = optionsRow, column = 4, pady=5)

        self.bouton_enr = Button(self.editeur, text='enregistrer', command=self.enregistrer)
        self.bouton_enr.grid(row = optionsRow, column = 0, pady=5)

        ##-----Creation des zones de texte-----##
        textRow=0
        self.texte = Label(self.editeur, text = "modeActuel")
        self.texte.grid(row = textRow, column = 0, columnspan=5)

        ##-----Creation des scrollbars-----##
        tilesetRow=2
        self.tilesetScrolly = Scrollbar(self.editeur)
        self.tilesetScrolly.grid(row=tilesetRow, column=2)

        self.tilesetScrollx = Scrollbar(self.editeur, orient='horizontal')
        self.tilesetScrollx.grid(row=tilesetRow+1, column=0)

        mapRow=2
        self.mapScrolly = Scrollbar(self.editeur)
        self.mapScrolly.grid(row=mapRow, column=4)

        self.mapScrollx = Scrollbar(self.editeur, orient='horizontal')
        self.mapScrollx.grid(row=mapRow+1, column=3)

        ##----- Creation des canevas -----##
        self.canvTileset = Canvas(self.editeur, width=400, height=600, bg='black', yscrollcommand = self.tilesetScrolly.set, xscrollcommand = self.tilesetScrollx.set)
        self.canvTileset.grid(row = tilesetRow, column = 0, padx=0, pady=5, columnspan=2)

        self.tilesetScrolly.config( command = self.canvTileset.yview )
        self.tilesetScrollx.config( command = self.canvTileset.xview )

        self.canvMap = Canvas(self.editeur, width=800, height=600, bg='white', yscrollcommand = self.mapScrolly.set, xscrollcommand = self.mapScrollx.set)
        self.canvMap.grid(row = mapRow, column = 3, padx=0, pady=5)

        """self.mapScrolly.config( command = self.canvMap.yview )
        self.mapScrollx.config( command = self.canvMap.xview )"""
        self.mapScrolly.config( command = self.mapYScroll )
        self.mapScrollx.config( command = self.mapXScroll )

        ##----- evenements -----##
        self.canvTileset.bind('<Button-1>', self.mouseChoiceTileset)
        self.canvTileset.bind('<ButtonRelease-1>', self.mouseReleaseTileset)
        self.canvMap.bind('<Button-1>', self.mouseChoiceMap)
        self.canvMap.bind('<B1-Motion>', self.mouseChoiceMap)
        self.canvMap.bind('<ButtonRelease-1>', self.mouseReleaseMap)

        ##-----Programme principal-----##
        #commencer()
        #self.modeToColision()

        #self.editeur.mainloop()

    def mapXScroll(self, a, b, c):
        self.hideMapScreen()
        self.canvMap.xview(a, b, c)
        #self.redrawMap()
        self.showMapScreen()

    def mapYScroll(self, a, b, c):
        self.hideMapScreen()
        self.canvMap.yview(a, b, c)
        #self.redrawMap()
        self.showMapScreen()

    def setBegin(self):
        self.loadGroundTileset()
        self.loadTileset()
        self.modeToColision()
        self.loadMap(1)
        """for x in range(self.mapXSize):
            for y in range(self.mapYSize):
                #self.map[x][y].displayedColision=ImageTk.PhotoImage(Image.open("imgs/v.png"))
                #self.map[x][y].displayedOver=ImageTk.PhotoImage(Image.open("imgs/v.png"))
                self.map[x][y].show()"""
        self.select=ImageTk.PhotoImage(Image.open("imgs/selected.png"))
        self.selectEnd=ImageTk.PhotoImage(Image.open("imgs/selectedEnd.png"))

    def mainloop(self):
        self.editeur.mainloop()

    def modeToColision(self):
        self.mode=mode.colision
        self.texte.configure(text="choix des colisions")
        self.redrawMap()

    def modeToSol(self):
        self.mode=mode.sol
        self.texte.configure(text="textures du sol")
        self.loadGroundTileset()
        self.showGroundTileset()
        self.redrawMap()
        self.selectId=self.canvTileset.create_image(0, 0, image=self.select, anchor = NW)
        self.selectEndId=self.canvTileset.create_image(0, 0, image=self.selectEnd, anchor = NW)
        self.choicex, self.choicexEnd, self.choicey, self.choiceyEnd=0, 1, 0, 1
        self.refreshChoice()

    def modeToTexture(self):
        self.mode=mode.texture
        self.texte.configure(text="textures de la 2e couche")
        self.loadTileset()
        self.showTileset()
        self.redrawMap()
        self.selectId=self.canvTileset.create_image(0, 0, image=self.select, anchor = NW)
        self.selectEndId=self.canvTileset.create_image(0, 0, image=self.selectEnd, anchor = NW)
        self.choicex, self.choicexEnd, self.choicey, self.choiceyEnd=0, 1, 0, 1
        self.refreshChoice()

    def modeToTexture3eC(self):
        self.mode=mode.texture3eC
        self.texte.configure(text="textures de la 3e couche")
        self.loadTileset()
        self.showTileset()
        self.redrawMap()
        self.selectId=self.canvTileset.create_image(0, 0, image=self.select, anchor = NW)
        self.selectEndId=self.canvTileset.create_image(0, 0, image=self.selectEnd, anchor = NW)
        self.choicex, self.choicexEnd, self.choicey, self.choiceyEnd=0, 1, 0, 1
        self.refreshChoice()

    def modeToOver(self):
        self.mode=mode.over
        self.texte.configure(text="choix des textures au dessus du joueur")
        self.redrawMap()

    def modeToEntity(self):
        self.mode=mode.entites
        self.texte.configure(text="creation des entites")
        self.redrawMap()


    def refreshMap(self):
        for x in range(self.mapXSize):
            for y in range(self.mapYSize):
                self.map[x][y].refreshTextures()

    def redrawMap(self):
        for x in range(self.mapXSize):
            for y in range(self.mapYSize):
                #self.map[x][y].refreshTextures()
                self.map[x][y].show()

    def hideMapScreen(self):
        xBegin=self.canvMap.canvasx(0)
        xEnd=xBegin+self.canvMap.winfo_width()
        xBegin=int(xBegin//self.tileSize)
        xEnd=int(xEnd//self.tileSize)+1
        xEnd=max(xEnd, self.mapXSize)
        yBegin=self.canvMap.canvasy(0)
        yEnd=yBegin+self.canvMap.winfo_height()
        yBegin=int(yBegin//self.tileSize)
        yEnd=int(yEnd//self.tileSize)+1
        yEnd=max(yEnd, self.mapYSize)
        for x in range(xBegin, xEnd):
            for y in range(yBegin, yEnd):
                #self.map[x][y].refreshTextures()
                self.map[x][y].hide()

    def showMapScreen(self):
        xBegin=self.canvMap.canvasx(0)
        #xEnd=xBegin+self.canvMap.winfo_width()
        xEnd=self.canvMap.canvasx(self.canvMap.winfo_width())
        xBegin=int(xBegin//self.tileSize)
        xEnd=int(xEnd//self.tileSize)+1
        xEnd=max(xEnd, self.mapXSize)
        yBegin=self.canvMap.canvasy(0)
        yEnd=yBegin+self.canvMap.winfo_height()
        yBegin=int(yBegin//self.tileSize)
        yEnd=int(yEnd//self.tileSize)+1
        yEnd=max(yEnd, self.mapYSize)
        for x in range(xBegin, xEnd):
            for y in range(yBegin, yEnd):
                #self.map[x][y].refreshTextures()
                self.map[x][y].show()

    def changeMap(self):
        """if self.changeMapWindow!=None:
            self.changeMapWindow.fen.destroy()
            self.changeMapWindow=None
            gc.collect()
            del self.changeMapWindow
        self.changeMapWindow=ChoixMap()
        self.changeMapWindow.mainloop()"""
        if self.changeMapWindow==None:
            self.changeMapWindow=ChoixMap()
            self.changeMapWindow.mainloop()
        else:
            self.changeMapWindow.show()

    def refreshMapCanvasSize(self):
        sr=(0, 0, self.mapXSize*self.tileSize, self.mapYSize*self.tileSize)
        self.canvMap.configure(scrollregion=sr)

    def refreshChoice(self):
        self.canvTileset.coords(self.selectId, self.choicex*self.tileSize, self.choicey*self.tileSize)
        self.canvTileset.coords(self.selectEndId, (self.choicexEnd-1)*self.tileSize, (self.choiceyEnd-1)*self.tileSize)

    def changeMapSize(self, x, y):
        map=[]
        for xx in range(x):
            map.append([])
            for yy in range(y):
                if xx < self.mapXSize and yy < self.mapYSize:#dans la map, garde l'ancienne tile
                    map[xx].append(self.map[xx][yy])
                else:#en dehors de la map, nouvelle tile
                    map[xx].append(Tile(xx, yy))
                    #map[xx][yy].changeColision(map[xx][yy].colision)
                    #map[xx][yy].changeOver(map[xx][yy].over)
        self.mapXSize=x
        self.mapYSize=y
        self.map=map
        self.refreshMapCanvasSize()
        self.redrawMap()
        self.allUnchanged()

        #self.showMap()

    def loadMap(self, id):
        self.mapId=id
        try:
            f=open(self.mapPath+"map{}.txt".format(id), "r")
            loaded=True
            print("loading map {}".format(id))
        except:
            loaded=False
            self.createMap()
            print("creating map {}".format(id))
        x=0
        y=0
        ent=None
        entAct=""
        if loaded:
            mode=0
            for line in f:
                line=line.strip()
                if line.startswith("}"):
                    if mode==6:
                        ent.act=entAct
                    mode=0
                    print("finished")
                elif mode==0:
                    if line.startswith("mapSize("):
                        line=line.replace("mapSize(", "")
                        line=line.replace(")", "")
                        self.mapXSize= int(line.split(";")[0].strip())
                        self.mapYSize= int(line.split(";")[1].strip())
                        self.createMap()
                        print("map size: {}, {}".format(self.mapXSize, self.mapYSize))
                    elif line.startswith("tilesets("):
                        line=line.replace("tilesets(", "")
                        line=line.replace(")", "")
                        self.groundTilesetName= line.split(";")[0].strip()
                        self.textureTilesetName= line.split(";")[1].strip()
                        self.loadTileset()
                        self.loadGroundTileset()
                        print("tilesets: {}, {}".format(self.groundTilesetName, self.textureTilesetName))
                        #self.createMap()
                        #print("map size: {}, {}".format(self.mapXSize, self.mapYSize))
                    elif line.startswith("map({"):
                        print("setting collisions")
                        mode=1
                        y=-1
                    elif line.startswith("groundTileset({"):
                        print("setting tiles' ground")
                        mode=2
                        y=-1
                    elif line.startswith("normalTileset({"):
                        print("setting tiles' sprite")
                        mode=3
                        y=-1
                    elif line.startswith("overTiles({"):
                        print("setting over tiles")
                        mode=4
                        y=-1
                    elif line.startswith("Textures3eC({"):
                        print("setting 3rd layout")
                        mode=5
                        y=-1
                    elif line.startswith("entity({"):
                        print("creating an entity")
                        mode=6
                        y=-1
                elif mode==1:
                    x=0
                    for c in line:
                        if c=="x":
                            self.map[x][y].changeColision(True)
                            #self.map[x][y].colision=True
                        else:
                            self.map[x][y].changeColision(False)
                            #self.map[x][y].colision=True
                        x+=1
                elif mode==2:
                    x=0
                    for c in line.split(","):
                        c=c.strip()
                        ts=self.groundTileset.size[0]//self.tileSize
                        self.map[x][y].changeSol(int(c)%ts, int(c)//ts)
                        """if c!="0":
                            print("YOOOO: {}, {}".format(int(c)%self.tileSize, int(c)//self.tileSize))"""
                        x+=1
                elif mode==3:
                    x=0
                    for c in line.split(","):
                        c=c.strip()
                        ts=self.textureTileset.size[0]//self.tileSize
                        #print(ts)
                        self.map[x][y].changeTexture(int(c)%ts, int(c)//ts)
                        """if c!="0":
                            print("YOOOO: {}, {}".format(int(c)%self.tileSize, int(c)//self.tileSize))"""
                        x+=1
                elif mode==4:
                    x=0
                    for c in line:
                        if c=="x":
                            self.map[x][y].changeOver(True)
                        else:
                            self.map[x][y].changeOver(False)
                        x+=1
                elif mode==5:
                    x=0
                    for c in line.split(","):
                        c=c.strip()
                        ts=self.textureTileset.size[0]//self.tileSize
                        #print(ts)
                        self.map[x][y].changeTexture3eC(int(c)%ts, int(c)//ts)
                        """if c!="0":
                            print("YOOOO: {}, {}".format(int(c)%self.tileSize, int(c)//self.tileSize))"""
                        x+=1
                elif mode==6:
                    x=0
                    if y==0:
                        args=line.split(";")
                        x=int(args[0])
                        y=int(args[1])
                        path=args[2].strip()
                        trav=stringToBool(args[3])
                        ent=Entite(x, y, "", path, trav)
                        entAct=""
                        self.map[x][y].entite=ent
                    else:
                        entAct+=line+"\n"
                y+=1
        #self.showMap()
        print("map loaded")
        self.hideMapScreen()
        self.editeur.title("editeur: map {}".format(self.mapId))
        self.refreshMapCanvasSize()
        #self.refreshMap()
        self.showMapScreen()
        #self.redrawMap()
        self.allUnchanged()
        print("map drew")


    def optionsMap(self):
        """o=OptionsMap()
        o.mainloop()"""
        if self.optionsWindow==None:
            self.optionsWindow=OptionsMap()
            self.optionsWindow.mainloop()
        else:
            self.optionsWindow.show()

    def enregistrer(self):
        entites=[]
        f = open(self.mapPath+"map{}.txt".format(self.mapId), "w")
        f.write("mapSize({};{})\n".format(self.mapXSize, self.mapYSize))
        f.write("tilesets({};{})\n".format(self.groundTilesetName, self.textureTilesetName))
        f.write("map({)\n")
        for y in range(self.mapYSize):
            for x in range(self.mapXSize):
                if(self.map[x][y].colision):
                    f.write("x")
                else:
                    f.write("0")
                ent=self.map[x][y].entite
                if ent!=None:
                    entites.append(ent)
            f.write("\n")
        f.write("}\n")

        f.write("groundTileset({)\n")
        for y in range(self.mapYSize):
            for x in range(self.mapXSize):
                f.write(str(self.map[x][y].sol))
                if x+1<self.mapXSize:
                    f.write(", ")
            f.write("\n")
        f.write("}\n")

        f.write("normalTileset({)\n")
        for y in range(self.mapYSize):
            for x in range(self.mapXSize):
                f.write(str(self.map[x][y].texture))
                if x+1<self.mapXSize:
                    f.write(", ")
            f.write("\n")
        f.write("}\n")

        f.write("Textures3eC({)\n")
        for y in range(self.mapYSize):
            for x in range(self.mapXSize):
                f.write(str(self.map[x][y].texture3eC))
                if x+1<self.mapXSize:
                    f.write(", ")
            f.write("\n")
        f.write("}\n")

        f.write("overTiles({)\n")
        for y in range(self.mapYSize):
            for x in range(self.mapXSize):
                if(self.map[x][y].over):
                    f.write("x")
                else:
                    f.write("0")
            f.write("\n")
        f.write("}\n")
        for ent in entites:
            print("sauvegarde entite")
            f.write("entity({)\n")
            f.write(ent.getInfos())
            f.write("}\n")
        print("map sauvegardée!")
        f.close()

    def loadGroundTileset(self):
        path=self.tilesetPath+self.groundTilesetName
        try:
            self.groundTileset = Image.open(path)
        except:
            print("le chargement de l'image a échoué")
            self.groundTileset=None

    def showGroundTileset(self):
        self.canvTileset.delete(ALL)
        if self.groundTileset!=None:
            self.displayedGroundTileset = ImageTk.PhotoImage(self.groundTileset)
            self.canvTileset.create_image(0, 0, image=self.displayedGroundTileset, anchor=NW)
            sr=(0, 0, self.groundTileset.size[0], self.groundTileset.size[1])
            self.canvTileset.configure(scrollregion=sr)
            #print(self.groundTilesetName+" affiché")

    def loadTileset(self):
        path=self.tilesetPath+self.textureTilesetName
        try:
            self.textureTileset = Image.open(path)
        except:
            print("le chargement de l'image a échoué")
            self.textureTileset=None

    def showTileset(self):
        self.canvTileset.delete(ALL)
        if self.textureTileset!=None:
            self.displayedTileset = ImageTk.PhotoImage(self.textureTileset)
            self.canvTileset.create_image(0, 0, image=self.displayedTileset, anchor=NW)
            sr=(0, 0, self.textureTileset.size[0], self.textureTileset.size[1])
            self.canvTileset.configure(scrollregion=sr)
            #print(self.textureTilesetName+" affiché")

    def createMap(self):
        self.map=[]
        """for x in range(self.mapXSize):
            self.map.append([])"""
        for x in range(self.mapXSize):
            self.map.append([])
            for y in range(self.mapYSize):
                self.map[x].append(Tile(x, y))
        #self.showMap()

    def showMap(self):
        for x in range(self.mapXSize):
            for y in range(self.mapYSize):
                print(self.map[x][y], end=" ")
            print()

    def mouseChoiceTileset(self, event):
        x = int(self.canvTileset.canvasx(event.x))
        y = int(self.canvTileset.canvasy(event.y))
        if x<0:
            x=0
        if y<0:
            y=0
        #x = event.x
        #y = event.y
        #print("clic: "+str(x)+" "+str(y))
        if self.mode==mode.texture or self.mode==mode.sol or self.mode==mode.texture3eC:
            if self.mode==mode.texture or self.mode==mode.texture3eC:
                img=self.textureTileset
            else:
                img=self.groundTileset
            self.choicex=x//self.tileSize
            self.choicey=y//self.tileSize
            maxX=img.size[0]//self.tileSize-1
            maxY=img.size[1]//self.tileSize-1
            if self.choicex>maxX:
                self.choicex=maxX
            if self.choicey>maxY:
                self.choicey=maxY
            print("choix debut: "+str(self.choicex)+", "+str(self.choicey))
            self.refreshChoice()

    def mouseReleaseTileset(self, event):
        x = int(self.canvTileset.canvasx(event.x))
        y = int(self.canvTileset.canvasy(event.y))
        if x<0:
            x=0
        if y<0:
            y=0
        #x = event.x
        #y = event.y
        #print("clic release: "+str(x)+" "+str(y))
        if self.mode==mode.texture or self.mode==mode.sol or self.mode==mode.texture3eC:
            if self.mode==mode.texture or self.mode==mode.texture3eC:
                img=self.textureTileset
            else:
                img=self.groundTileset
            self.choicexEnd=(x//self.tileSize)
            self.choiceyEnd=(y//self.tileSize)
            maxX=img.size[0]//self.tileSize-1
            maxY=img.size[1]//self.tileSize-1
            if self.choicexEnd>maxX:
                self.choicexEnd=maxX
            if self.choiceyEnd>maxY:
                self.choiceyEnd=maxY
            if self.choicexEnd<=self.choicex:
                self.choicexEnd=self.choicex
            if self.choiceyEnd<=self.choicey:
                self.choiceyEnd=self.choicey
            self.choicexEnd+=1
            self.choiceyEnd+=1
            print("choix fin: "+str(self.choicexEnd)+", "+str(self.choiceyEnd))
            self.refreshChoice()

    def mouseChoiceMap(self, event):
        x = int(self.canvMap.canvasx(event.x))
        y = int(self.canvMap.canvasy(event.y))
        if x<0:
            x=0
        if y<0:
            y=0
        #x = event.x
        #y = event.y
        #print("clic: "+str(x)+" "+str(y))
        x=x//self.tileSize
        y=y//self.tileSize
        if x>=self.mapXSize:
            x=self.mapXSize-1
        if y>=self.mapYSize:
            y=self.mapYSize-1
        if self.firstClickMap:
            self.firstClickX=x
            self.firstClickY=y
            print("first click: {}, {}".format(x, y))
        if self.mode==mode.texture or self.mode==mode.sol or self.mode==mode.texture3eC:
            if self.mode==mode.texture:
                newX=self.choicex+((x-self.firstClickX)%(self.choicexEnd-self.choicex))
                newY=self.choicey+((y-self.firstClickY)%(self.choiceyEnd-self.choicey))
                #self.map[x][y].changeTexture(self.choicex, self.choicey)
                self.map[x][y].changeTexture(newX, newY, True)
            elif self.mode==mode.texture3eC:
                newX=self.choicex+((x-self.firstClickX)%(self.choicexEnd-self.choicex))
                newY=self.choicey+((y-self.firstClickY)%(self.choiceyEnd-self.choicey))
                #self.map[x][y].changeTexture(self.choicex, self.choicey)
                self.map[x][y].changeTexture3eC(newX, newY, True)
            else:
                #print(str(x)+", "+ str(y))
                newX=self.choicex+((x-self.firstClickX)%(self.choicexEnd-self.choicex))
                newY=self.choicey+((y-self.firstClickY)%(self.choiceyEnd-self.choicey))
                self.map[x][y].changeSol(newX, newY, True)
        elif self.mode==mode.colision:
            tile=self.map[x][y]
            if tile.alreadyChanged==False:
                tile.changeColision(tile.colision==False, True)
                print("change")
            else:
                print("change pas")
        elif self.mode==mode.over:
            tile=self.map[x][y]
            if tile.alreadyChanged==False:
                tile.changeOver(tile.over==False, True)
        elif self.mode==mode.entites and self.firstClickMap and self.modifierFerme():
            self.openEntity(self.map[x][y].entite, x, y)
        self.firstClickMap=False

    def modifierFerme(self):
        if self.entityModifier==None:
            print("pas de modifier")
            return True
        elif self.entityModifier.opened:
            print("deja ouvert")
            return False
        else:
            print("pas ouvert")
            return True

    def openEntity(self, ent, x, y):
        if self.entityModifier==None:
            self.entityModifier=EntiteModifier(ent, x, y)
            self.entityModifier.mainloop()
        else:
            self.entityModifier.show(x, y)

    def mouseReleaseMap(self, event):
        self.allUnchanged()
        self.firstClickMap=True

    def allUnchanged(self):
        for x in range(self.mapXSize):
            for y in range(self.mapYSize):
                self.map[x][y].alreadyChanged=False

    def quitter(self):
        self.editeur.destroy()
        try:
            self.changeMapWindow.fen.destroy()
            self.changeMapWindow.fen.quit()
        except:
            pass
        try:
            self.optionsWindow.fen.destroy()
            self.optionsWindow.fen.quit()
        except:
            pass
        try:
            self.entityModifier.fen.destroy()
            self.entityModifier.fen.quit()
        except:
            pass
        #sys.exit(0)


class ChoixMap:
    def __init__(self):
        ##-----Creation de la fenetre-----##
        self.fen=Tk()
        self.fen.title("Choix de la map")
        self.fen.geometry("200x200")
        self.fen.resizable(0, 0)

        ##-----Creation des boutons-----##
        boutonRow=2
        self.bouton_colisions = Button(self.fen, text='valider', command=self.valide)
        self.bouton_colisions.grid(row = boutonRow, column = 0, pady=5)

        self.bouton_sol = Button(self.fen, text='annuler', command=self.annule)
        self.bouton_sol.grid(row = boutonRow, column = 1, pady=5)

        ##-----Creation des entrees-----##
        choiceRow=1
        self.entrId = Entry(self.fen)
        self.entrId.grid(row=choiceRow, column=0, padx=3, pady=2, columnspan=2)

        ##-----Création des zones de texte-----##
        textRow=0
        self.textedefb=Label(self.fen, text="Id de la map")
        self.textedefb.grid(row=textRow, column=0, padx=5, pady=5, columnspan=2)

    def mainloop(self):
        self.fen.mainloop()

    def valide(self):
        global m
        id=int(self.entrId.get())
        m.loadMap(id)
        self.hide()

    def annule(self):
        self.hide()

    def show(self):
        self.fen.update()
        self.fen.deiconify()

    def hide(self):
        self.fen.withdraw()

class OptionsMap:
    def __init__(self):
        global m
        ##-----Creation de la fenetre-----##
        self.fen=Tk()
        self.fen.title("Options de la map")
        self.fen.resizable(0, 0)

        ##-----Creation des boutons-----##
        boutonRow=2
        self.bouton_colisions = Button(self.fen, text='valider', command=self.valide)
        self.bouton_colisions.grid(row = boutonRow, column = 0, pady=5, columnspan=2)

        self.bouton_sol = Button(self.fen, text='annuler', command=self.annule)
        self.bouton_sol.grid(row = boutonRow, column = 2, pady=5, columnspan=2)

        ##-----Creation des entrees-----##
        choiceRow=1
        self.entrxSize = Entry(self.fen)
        self.entrxSize.grid(row=choiceRow, column=0, padx=3, pady=2)
        #self.entrxSize.insert(0, str(m.mapXSize))

        self.entrySize = Entry(self.fen)
        self.entrySize.grid(row=choiceRow, column=1, padx=3, pady=2)
        #self.entrySize.insert(0, str(m.mapYSize))

        self.entrGrTset = Entry(self.fen)
        self.entrGrTset.grid(row=choiceRow, column=2, padx=3, pady=2)
        #self.entrGrTset.insert(0, m.groundTilesetName)

        self.entrTset = Entry(self.fen)
        self.entrTset.grid(row=choiceRow, column=3, padx=3, pady=2)
        #self.entrTset.insert(0, m.textureTilesetName)

        self.setEntries()

        ##-----Création des zones de texte-----##
        textRow=0
        self.textxSize=Label(self.fen, text="taille X de la map")
        self.textxSize.grid(row=textRow, column=0, padx=5, pady=5)

        self.textySize=Label(self.fen, text="taille Y de la map")
        self.textySize.grid(row=textRow, column=1, padx=5, pady=5)

        self.textGrTset=Label(self.fen, text="tileset du sol")
        self.textGrTset.grid(row=textRow, column=2, padx=5, pady=5)

        self.textTset=Label(self.fen, text="tileset normal")
        self.textTset.grid(row=textRow, column=3, padx=5, pady=5)

    def mainloop(self):
        self.fen.mainloop()

    def valide(self):
        print("valide")
        global m

        x=int(self.entrxSize.get())
        y=int(self.entrySize.get())
        m.changeMapSize(x, y)


        m.groundTilesetName=self.entrGrTset.get()
        m.textureTilesetName=self.entrTset.get()
        m.loadGroundTileset()
        m.loadTileset()

        #m.refreshMap()

        m.redrawMap()

        self.hide()

    def annule(self):
        self.hide()

    def show(self):
        self.cleanEntries()
        self.setEntries()
        self.fen.update()
        self.fen.deiconify()

    def hide(self):
        self.fen.withdraw()

    def cleanEntries(self):
        self.entrxSize.delete(0,END)
        self.entrySize.delete(0,END)
        self.entrGrTset.delete(0,END)
        self.entrTset.delete(0,END)

    def setEntries(self):
        global m
        self.entrxSize.insert(0, str(m.mapXSize))
        self.entrySize.insert(0, str(m.mapYSize))
        self.entrGrTset.insert(0, m.groundTilesetName)
        self.entrTset.insert(0, m.textureTilesetName)

class EntiteModifier:
    def __init__(self, entite, x, y):
        global m

        self.opened=True
        self.entite=None
        self.changeEntite(entite, x, y)

        ##-----Creation de la fenetre-----##
        self.fen=Tk()
        self.fen.title("Options de l'entite")
        self.fen.resizable(0, 0)

        ##-----Creation des entrees-----##
        choiceRow=1
        self.entrxPos = Entry(self.fen)
        self.entrxPos.grid(row=choiceRow, column=0, padx=3, pady=2)

        self.entryPos = Entry(self.fen)
        self.entryPos.grid(row=choiceRow, column=1, padx=3, pady=2)

        self.entrSpritePath = Entry(self.fen)
        self.entrSpritePath.grid(row=choiceRow, column=2, padx=3, pady=2)

        self.entrTrav = Entry(self.fen)
        self.entrTrav.grid(row=choiceRow, column=3, padx=3, pady=2)

        self.entrActions = Text(self.fen)
        self.entrActions.grid(row=choiceRow+1, column=0, columnspan=4)

        ##-----Création des zones de texte-----##
        textRow=0
        self.textxPos=Label(self.fen, text="position X")
        self.textxPos.grid(row=textRow, column=0, padx=5, pady=5)

        self.textyPos=Label(self.fen, text="position Y")
        self.textyPos.grid(row=textRow, column=1, padx=5, pady=5)

        self.textSpritePath=Label(self.fen, text="chemin du sprite")
        self.textSpritePath.grid(row=textRow, column=2, padx=5, pady=5)

        self.textTrav=Label(self.fen, text="traversable")
        self.textTrav.grid(row=textRow, column=3, padx=5, pady=5)

        ##-----Creation des boutons-----##
        boutonRow=3
        self.bouton_valide = Button(self.fen, text='valider', command=self.valide)
        self.bouton_valide.grid(row = boutonRow, column = 0, pady=5, columnspan=2)

        self.bouton_annule = Button(self.fen, text='annuler', command=self.annule)
        self.bouton_annule.grid(row = boutonRow, column = 2, pady=5, columnspan=2)

        self.refreshInfos()

        self.fen.protocol("WM_DELETE_WINDOW", self.annule)

    def valide(self):
        global m
        try:
            oldx=self.entite.posx
            oldy=self.entite.posy
            self.updateEntite()
            m.map[oldx][oldy].entite=None
            newx=self.entite.posx
            newy=self.entite.posy
            if m.map[newx][newy].entite!=None:
                swap=m.map[newx][newy].entite
                m.map[newx][newy].entite=self.entite
                m.map[oldx][oldy].entite=swap
            else:
                m.map[newx][newy].entite=self.entite
            self.hide()
            m.hideMapScreen()
            m.showMapScreen()
        except Exception as e:
            print("erreur: "+str(e))

    def updateEntite(self):
        ent=self.entite
        ent.posx=int(self.entrxPos.get())
        ent.posy=int(self.entryPos.get())
        ent.spritePath=self.entrSpritePath.get()
        ent.traverse=stringToBool(self.entrTrav.get(), err=True)
        ent.act=self.entrActions.get("1.0", "end")

    def annule(self):
        self.hide()

    def show(self, x, y):
        global m
        self.opened=True
        self.changeEntite(m.map[x][y].entite, x, y)
        self.refreshInfos()
        self.fen.update()
        self.fen.deiconify()

    def hide(self):
        self.opened=False
        self.fen.withdraw()

    def changeEntite(self, ent, x, y):
        self.entite=ent
        if ent==None:
            self.entite=Entite(x, y, "", "")

    def cleanEntries(self):
        self.entrxPos.delete(0,END)
        self.entryPos.delete(0,END)
        self.entrSpritePath.delete(0,END)
        self.entrTrav.delete(0,END)
        self.entrActions.delete("1.0", "end")

    def setEntries(self):
        global m
        self.entrxPos.insert(0, str(self.entite.posx))
        self.entryPos.insert(0, str(self.entite.posy))
        self.entrSpritePath.insert(0, self.entite.spritePath)
        self.entrTrav.insert(0, str(self.entite.traverse))
        self.entrActions.insert("1.0", self.entite.act)

    def refreshInfos(self):
        self.cleanEntries()
        self.setEntries()


    def mainloop(self):
        self.fen.mainloop()



m = Main()
m.setBegin()
m.mainloop()
