package Controller;

import com.badlogic.gdx.Input.Keys;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;

import Dialogue.Dialogue;
import Dialogue.DialogueBox;
import Dialogue.DialogueNode;
import Dialogue.DialogueTraverser;
import Dialogue.OptionBox;
import Dialogue.DialogueNode.NODE_TYPE;
import main.Hacker;
import model.Actor;
import model.Direction;
import screen.GameScreen;
import screen.TaquinScreen;
import screen.ScreenManager.STATE;


public class DialogueController extends InputAdapter {
	private Actor player;
	private DialogueTraverser traverser;
	private DialogueBox dialogueBox;
	private OptionBox optionBox;
	private DialogueNode dialogueNode;
	 private List<String> tableau =  new ArrayList<String>();
	 private Hacker hacker;
	
	
	
    private boolean test = true;

	public DialogueController(DialogueBox box, OptionBox optionBox) {
		this.dialogueBox = box;
		this.optionBox = optionBox;
		tableau.add("Kanagawa");
		tableau.add("Osaka");
		tableau.add("Centre Ville");
		tableau.add("Jardin Zen");
		
	}
	
	@Override
	public boolean keyDown(int keycode) {
		if (dialogueBox.isVisible()) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean keyUp(int keycode) {
		if (optionBox.isVisible()) {
			if (keycode == Keys.UP) {

				optionBox.moveUp();
				return true;
			} else if (keycode == Keys.DOWN) {
				optionBox.moveDown();
				return true;
			}
		}
		
	

		if (traverser != null && keycode == Keys.X && dialogueBox.isFinished()) { // continue through tree
			if (traverser.getOptions().equals(tableau) ) {
				if (optionBox.getSelected() ==0){
					Hacker.getGameScreen().tp(49,24);
			        	
					}
				if (optionBox.getSelected() == 1){
				Hacker.getGameScreen().tp(74,40);
		        	
				}
				if (optionBox.getSelected() == 2){
					Hacker.getGameScreen().tp(41,54);
			        	
					}
				if (optionBox.getSelected() == 3){
					Hacker.getGameScreen().tp(33,67);
			        	
					}
			}
				if (traverser.getType()== NODE_TYPE.END) {
					traverser = null;
					dialogueBox.setVisible (false);
				}
				else if (traverser.getType()== NODE_TYPE.LINEAR) {
					progress(0);
				}
				else if (traverser.getType() == NODE_TYPE.MULTIPLE_CHOICE) {
					progress(optionBox.getIndex());
				}
				return true ;
		}
		if (dialogueBox.isVisible()) {
			return true;
		}
		return false;
	}
	
	public void update(float delta) {
		if (dialogueBox.isFinished() && traverser != null) {
			if (traverser.getType() == NODE_TYPE.MULTIPLE_CHOICE) {
				optionBox.setVisible(true);
			}
		}
	}
	
	public void startDialogue(Dialogue dialogue) {
		traverser = new DialogueTraverser(dialogue);
		dialogueBox.setVisible(true);
		dialogueBox.animateText(traverser.getText());
		
		if (traverser.getType() == NODE_TYPE.MULTIPLE_CHOICE) {
			optionBox.clear();
			for (String s :dialogue.getNode(dialogue.getStart()).getLabels()) {
				optionBox.addOption(s);
			}
		}
		
	}
	
	private void progress(int index) {
		optionBox.setVisible(false);
		DialogueNode nextNode = traverser.getNextNode(index);
		dialogueBox.animateText(nextNode.getText());

		if (nextNode.getType() == NODE_TYPE.MULTIPLE_CHOICE)  {
			optionBox.clearChoices();
		}
		
			for (String s : nextNode.getLabels()) {
				optionBox.addOption(s);
			}
		}
	
	
	public boolean isDialogueShowing() {
		return dialogueBox.isVisible();
	}
}