package Controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;

import Dialogue.DialogueBox;
import main.Hacker;
import model.Actor;
import model.Direction;
import model.Player;
import screen.AnimationCases;
import screen.GameScreen;
import screen.Menu;
import screen.ScreenManager;
import screen.ScreenManager.STATE;
import screen.SpaceScreen;
import screen.TaquinScreen;

public class PlayerController extends InputAdapter {

	
	private GameScreen game;
	private Player player;
    private boolean [] buttonPress;
    private float [] buttonTimer;
    public static int healthBar =0;
    public static int lvl =0;
   
    private float WALK_REFACE_THRESHOLD = 0.06f;	
	public PlayerController(Player player) {
        this.player = player;
        buttonPress = new boolean [Direction.values().length];
        buttonPress[Direction.NORTH.ordinal()]=false;
        buttonPress[Direction.SOUTH.ordinal()]=false;
        buttonPress[Direction.EAST.ordinal()]=false;
        buttonPress[Direction.WEST.ordinal()]=false;
        
        buttonTimer = new float [Direction.values().length];
        buttonTimer[Direction.NORTH.ordinal()]=0f;
        buttonTimer[Direction.SOUTH.ordinal()]=0f;
        buttonTimer[Direction.EAST.ordinal()]=0f;
        buttonTimer[Direction.WEST.ordinal()]=0f;

        
        
        
        
        
    }

    @Override
    public boolean keyDown(int keycode) {
        if (!buttonPress[Direction.NORTH.ordinal()] && (keycode== Keys.UP || keycode== Keys.Z)){
            buttonPress[Direction.NORTH.ordinal()]=true;


        }
        if (!buttonPress[Direction.SOUTH.ordinal()] && (keycode== Keys.DOWN  || keycode== Keys.S)){

            buttonPress[Direction.SOUTH.ordinal()]=true;

        }
        if (!buttonPress[Direction.WEST.ordinal()] && (keycode== Keys.LEFT  || keycode== Keys.Q)){


            buttonPress[Direction.WEST.ordinal()]=true;

        }
        if (!buttonPress[Direction.EAST.ordinal()] && ( keycode== Keys.RIGHT  || keycode== Keys.D)){

            buttonPress[Direction.EAST.ordinal()]=true;
        }
        if (keycode== Keys.C)
        {
        	if (model.Camera.isCameraState() == false) {model.Camera.setCameraState(true);} else model.Camera.setCameraState(false);
        }
        
        if (keycode== Keys.T){
        	if( healthBar>4) {
        		healthBar=0;
        		lvl++;
        	}else healthBar++;
        }
        /*
        if (keycode== Keys.I){
        	
        	//Gdx.input.setInputProcessor(SpaceScreen.stage);
        	Hacker.gsm.setScreens(STATE.Space);
        }
        
        if (keycode== Keys.P){
        	Hacker.gsm.setScreens(STATE.Anim);
        	Gdx.input.setInputProcessor(AnimationCases.animStage);
        }
        
        if (keycode== Keys.M){

        	Hacker.gsm.setScreens(STATE.Taquin);
        	Gdx.input.setInputProcessor(TaquinScreen.stage);
        }
        */
        if (keycode== Keys.ESCAPE){
        	
       
	
        	if (ScreenManager.getState() == STATE.Quest)
        	{
            	Hacker.gsm.setScreens(STATE.PLAY);

        
        	}else {
        		GameScreen.gameViewPort.setScreenSize(1280, 720);
        	GameScreen.gameViewPort.apply();
            Hacker.gsm.setScreens(STATE.Menu);
            Gdx.input.setInputProcessor(Menu.stage);
        	}
            }
        
        
        if (keycode== Keys.O){
        	Hacker.gsm.setScreens(STATE.Quest);
        	System.out.print(ScreenManager.getState());
        	
        }
        if (keycode== Keys.ENTER){
        	int x=(int)player.getPosX()+player.getFacing().getDx();
        	int y=(int)player.getPosY()+player.getFacing().getDy();
        	System.out.println("interaction avec "+x+", "+y);
        	if(player.getMap().isInMap(x,  y)){
        		player.getMap().getTile(x, y).executeEntities();
        	}
        }

        return false;
    }

    
    @Override
    public boolean keyUp(int keycode) {
        if (keycode== Keys.UP || keycode== Keys.Z){
        	releaseDirection (Direction .NORTH);
        }
        if (keycode== Keys.DOWN || keycode== Keys.S){
        	releaseDirection (Direction .SOUTH);

        }
        if (keycode== Keys.LEFT || keycode== Keys.Q){
        	releaseDirection (Direction .WEST);
        }
        if (keycode== Keys.RIGHT || keycode== Keys.D){
        	releaseDirection (Direction .EAST);
        }
       
        return false;
    }
    
    public void update (float delta) {
    	if(buttonPress[Direction.NORTH.ordinal()]) {
    		updateDirection(Direction.NORTH,delta	);
    		return ;
    	}
    	if(buttonPress[Direction.SOUTH.ordinal()]) {
    		updateDirection(Direction.SOUTH,delta);
    		return ;
    	}
    	if(buttonPress[Direction.EAST.ordinal()]) {
    		updateDirection(Direction.EAST,delta);
    		return ;
    	}
    	if(buttonPress[Direction.WEST.ordinal()]) {
    		updateDirection(Direction.WEST,delta);
    		return ;
    	}
    			
    }
    
    private void updateDirection (Direction dir, float delta) {
    	buttonTimer[dir.ordinal()]+=delta;
    	considerMove(dir);
    	
    }
    private void releaseDirection (Direction dir) {
    	buttonPress[dir.ordinal()]=false;
    	considerReface(dir);
    	buttonTimer[dir.ordinal()]=0f;
    	
    }
    
	private void considerMove(Direction dir) {
		if(buttonTimer[dir.ordinal()]>WALK_REFACE_THRESHOLD){
			//System.out.println("move!");
			player.move(dir);
		}
	}
    
	private void considerReface(Direction dir) {
		if(buttonTimer[dir.ordinal()]<WALK_REFACE_THRESHOLD){
			player.reface(dir);
		}
	}
	

	public int getHealthBar() {
		// TODO Auto-generated method stub
		return healthBar;
 
	}
	public int getLvl() {
		// TODO Auto-generated method stub
		return lvl;
 
	}
    

}
