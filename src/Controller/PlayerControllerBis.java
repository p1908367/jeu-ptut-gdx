package Controller;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import model.Actor;
import model.Direction;
import model.Player;

public class PlayerControllerBis extends InputAdapter {
    private Player player;

    public PlayerControllerBis(Actor actor) {
        this.player = player;
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode== Input.Keys.UP){
            //player.move(0,1);
            player.move(Direction.NORTH);

        }
        if (keycode== Input.Keys.DOWN){
            //player.move(0,-1);
            player.move(Direction.SOUTH);


        }
        if (keycode== Input.Keys.LEFT){
            //player.move(-1,0);
            player.move(Direction.WEST);
        }
        if (keycode== Input.Keys.RIGHT){
            //player.move(1,0);
            player.move(Direction.EAST);

        }
        if (keycode== Input.Keys.C){
            model.Camera.setCameraState(false);
        }
        
        if (keycode== Input.Keys.S){
            save.Save.saveGame();
            System.out.println(screen.GameScreen.getPlayer().getX());
        }
        
        if (keycode== Input.Keys.L){
            save.Save.loadSave();
        }




        return false;
    }

}
