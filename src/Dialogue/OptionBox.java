package Dialogue;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

public class OptionBox extends Table {
	
	private int selectorIndex = 0;
	private String option;
	private List<Image> arrows = new ArrayList<Image>();
	private List<Label> options = new ArrayList<Label>();
	
	private Table uiContainer;
	
	public OptionBox(Skin skin) {
		super(skin);
		this.setBackground("optionbox");
		uiContainer = new Table();
		this.add(uiContainer).pad(8f);
	}
	
	public void addOption(String option) {
		this.option=option;
		Label optionLabel = new Label(option, this.getSkin());
		options.add(optionLabel);
		Image arrow = new Image(this.getSkin(), "arrow");
		arrow.setVisible(false);
		arrows.add(arrow);
		
		
		
		uiContainer.add(arrow).expand().align(Align.left);
		uiContainer.add(optionLabel)
							.expand()
							.align(Align.left)
							.space(8f);
		uiContainer.row();
		calcArrowVisibility();
		
	}
	
	
	private void calcArrowVisibility() {
		for (int i=0 ; i< arrows.size();i++) {
			if (i==selectorIndex) {
				arrows.get(i).setVisible(true);
			}else {
				arrows.get(i).setVisible(false);
			}
		}
	}

	public void moveUp() {
		selectorIndex--;

		calcArrowVisibility();
		if (selectorIndex < 0) {
			selectorIndex = 0;
		}
		
		calcArrowVisibility();

	}
	
	public void moveDown() {
		selectorIndex++;

		if (selectorIndex >= options.size()) {
			selectorIndex = options.size()-1;
		}
		
		
		calcArrowVisibility();

	}
	public int getSelected() {
		return selectorIndex;
	}
	public void clearChoices() {
		uiContainer.clearChildren();
		options.clear();
		arrows.clear();
		selectorIndex = 0;
	}
	
	public int getIndex() {
		return selectorIndex;
	}
	
	/**
	 * @return	Number of options added to this OptionBox
	 */
	public int getAmount() {
		return options.size();
	}

	
}
