package Utile;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class HealthBar extends Table {
	
	public HealthBar (Skin skin) {
		super(skin);
		this.setBackground("hpbar_bar");

	}
}
