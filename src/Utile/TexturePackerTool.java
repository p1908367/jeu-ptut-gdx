package Utile;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class TexturePackerTool  {
	public static void main(String[] args) {
		TexturePacker.process(
				"assets/graphics_unpacked/",
				"assets/graphics_packed/test/",
				"textures");
	}

}
