package health_level_money_bar;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Health_Bar extends ApplicationAdapter{

	    /*SpriteBatch batch;
	     int i=0;
	    Texture texture,texture2;
	    @Override
	    public void create () {
	        batch = new SpriteBatch();

	        initTestObjects() ;
	    }

	    @Override
	    public void render () {
	        Gdx.gl.glClearColor(0, 0, 0, 0);
	        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	        batch.begin();


	        batch.draw(texture2,100,100,300,20);
	    batch.draw(texture,100,100,i,20);
	    if(i>300)
	    {
	        i=0;
	    }
	    i++;

	        batch.end();
	    }
	    private void initTestObjects() {

	        int width =1 ;
	        int height = 1;
	        Pixmap pixmap = createProceduralPixmap(width, height,0,1,0);
	        Pixmap pixmap2 = createProceduralPixmap(width, height,1,0,0);

	    texture = new Texture(pixmap);
	    texture2 = new Texture(pixmap2);



	        }



	    private Pixmap createProceduralPixmap (int width, int height,int r,int g,int b) {
	        Pixmap pixmap = new Pixmap(width, height, Format.RGBA8888);

	        pixmap.setColor(r, g, b, 1);
	        pixmap.fill();

	        return pixmap;
	        }
	}*/
	

	private double health_bar = 0;
	SpriteBatch affichage_bar_vie; //ce qui permet d'afficher sur l'ecran
	Texture image;
	
	public Health_Bar() {
		this.health_bar = 5;
	}
	
	@Override
	public void create() //methode d'initialisation de l'affichage
	{
		affichage_bar_vie = new SpriteBatch();
		image = new Texture("/health_bar.png"); //image
	}
	
	@Override
	public void render() //methode prioncipale qui s'execute en boucle
	{
		affichage_bar_vie.begin();
		affichage_bar_vie.draw(image, 0, 0);
		affichage_bar_vie.end();
	}
	
	
	@Override
	public void dispose() //methode qui nettoie les buffers utilis�s pour l'affichage
	{
		affichage_bar_vie.dispose();
		image.dispose();
	}
	
}
