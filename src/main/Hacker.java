package main;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import Transition.Transition;
import Utile.Action;
import screen.AbstractScreen;
import screen.AnimationCases;
import screen.GameScreen;
import screen.Menu;
import screen.ScreenManager;
import screen.ScreenManager.STATE;
import screen.ShopScreen;
import screen.SpaceScreen;
import screen.TaquinScreen;
import screen.TransitionScreen;

public class Hacker extends Game {
	 //manager
    public static  ScreenManager gsm;
    public AssetManager assets;
    //batches
    public SpriteBatch batch;
    public ShapeRenderer shapeBatch;
    private static GameScreen screen;
    private TaquinScreen taquin;

	private TransitionScreen transitionScreen;
	private Menu menu;
	private SpaceScreen spaceScreen;
	private ShopScreen shop;
	private AnimationCases animcase;
    
    
    
    @Override
    public void create() {
    	 assets= new AssetManager();
         assets.load("assets/graphics_packed/textures.atlas",TextureAtlas.class);
         assets.load("assets/graphics_packed/ui/uipack.atlas",TextureAtlas.class);
         assets.load("assets/font/small_letters_font.fnt", BitmapFont.class);
         assets.load("assets/minecraft-theme-song-extended-for-30-minutes.mp3", Music.class);
         assets.load("assets/bar1.png", Texture.class);
         assets.load("assets/bar2.png", Texture.class);
         assets.load("assets/bar3.png", Texture.class);
         assets.load("assets/bar4.png", Texture.class);
         assets.load("assets/bar5.png", Texture.class);
         assets.load("assets/bar6.png", Texture.class);
         assets.load("assets/white.png", Texture.class);


         
         assets.finishLoading();
         batch = new SpriteBatch();
         shapeBatch= new ShapeRenderer();
        screen = new GameScreen(this);
        transitionScreen = new TransitionScreen(this);
        taquin = new TaquinScreen(this);
        menu = new Menu(this);
        shop = new ShopScreen(this);
        animcase = new AnimationCases(this);
        
        spaceScreen = new SpaceScreen(this);
        gsm = new ScreenManager(this);
     
         
    }
    
    @Override
    public void render(){
        super.render();
        if(getScreen() instanceof AbstractScreen) {
        	((AbstractScreen)getScreen()).update(Gdx.graphics.getDeltaTime());
        }
        
        Gdx.gl.glClearColor(1f,1f,1f,1f);  //c'est pour effacer l'ecran entre 2 ecran
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);               //c'est pour effacer l'ecran entre 2 ecran
        getScreen().render(Gdx.graphics.getDeltaTime());
     
    }
    
    public void startTransition(AbstractScreen from, AbstractScreen to, Transition out, Transition in, Action action) {
		transitionScreen.startTransition(from, to, out, in, action);
	}
    
   
    public  AssetManager getAssetManager() {	
	return assets;

    }
    public static GameScreen getGameScreen() {
		return screen;
	}
	public TaquinScreen getTaquin() {
		return taquin;
	}
	public Menu getMenu() {
		return menu;
	}

	public ShopScreen getShop() {
		return shop;
	}
	
	public AnimationCases getAnimCases() {
		return animcase;
	}

	public SpaceScreen getSpaceScreen() {
		return spaceScreen;
	}
	
	public static void launchMiniGame(int id) {
		/**lance un mini jeu en fonction de l'id:
		 * 0=taquin
		 * 1=memoire
		 * 2=space shooter
		 */
		if(id==0) {//taquin
        	Hacker.gsm.setScreens(STATE.Taquin);
        	Gdx.input.setInputProcessor(TaquinScreen.stage);
		} else if(id==1) {//memoire
        	Hacker.gsm.setScreens(STATE.Anim);
        	Gdx.input.setInputProcessor(AnimationCases.animStage);
		} else if(id==2) {//space shooter
        	Hacker.gsm.setScreens(STATE.Space);
		}
	}
	
	public static void launchShop() {
    	Hacker.gsm.setScreens(STATE.Shop);
    	Gdx.input.setInputProcessor(ShopScreen.shopStage);
	}
}
