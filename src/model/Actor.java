package model;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;

import Controller.PlayerController;
import Dialogue.DialogueBox;
import Utile.AnimationSet;
import screen.GameScreen;

public class Actor {


	

	public static TileMap map;

	    private int x;
	    private int y;
	    private float worldX,worldY;
	    private Direction facing;

	    private int srcX,srcY;
	    private int destX,destY;
	    private float animTimer;
	    private float ANIM_TIME=0.3f;
	    private float REFACE_TIMER= 0.1f;
	    private ACTOR_STATE state;
	    private float walkTimer;
	    private boolean moveRequestThisFrame;
	    private AnimationSet animations;
	    private DialogueBox box;
	    
	    
	    public Actor(TileMap map,int x, int y, AnimationSet animations) {
	        this.map=map;
	        this.x = x;
	        this.y = y;
	        this.worldX=x;
	        this.worldY=y;
	        this.animations=animations;
	        //map.getTile(x,y).addEntity(this);
	        this.state=ACTOR_STATE.STANDING;
	        
	        this.facing= Direction.SOUTH;

	    }
	    public enum ACTOR_STATE{
	        WALKING,
	        STANDING,
	        REFACING;
	    }
	    public void update(float delta){
	        if(state==ACTOR_STATE.WALKING){
	            animTimer+=delta;
	            walkTimer+=delta;
	            worldX= Interpolation.linear.apply(srcX,destX,animTimer/ANIM_TIME);
	            worldY=Interpolation.linear.apply(srcY,destY,animTimer/ANIM_TIME);
	            if(animTimer>ANIM_TIME){
	            	float leftOvertime =animTimer-ANIM_TIME;
	            	walkTimer-=leftOvertime;
	                finishMove();
	                if(moveRequestThisFrame) {
	                	move(facing) ;
	    	            	
	                	
	                }else {
	                	walkTimer=0f;
	                }
	            }
	        }	
	        if (state== ACTOR_STATE.REFACING) {
	        	animTimer+=delta;
	        	if (animTimer >REFACE_TIMER) {
	        		state=ACTOR_STATE.STANDING;
	        	}
	        }
	        moveRequestThisFrame=false;
	    }
	    public boolean move (Direction dir){
	        if (state==ACTOR_STATE.WALKING){
	        	if (facing ==dir) {
	            moveRequestThisFrame =true;}
	        	return false;
	        }
	        else
	        	this.facing=dir;//so that the he faces the direction even if it is not passable
	        if (x+dir.getDx()>=getMap().getWidth() || x+dir.getDx()<0 || y+dir.getDy() >=getMap().getHeight() || y+dir.getDy() <0){
	            return false;
	        }
	        
	        //if (getMap().getTile(x+dir.getDx(),y+dir.getDy()).getActor()!=null){
	          //  return false;
	        //}
	        if (getMap().getTile(x+dir.getDx(),y+dir.getDy()).passable()==false){
	            return false;
	        }
	        if(x+dir.getDx()>=140) {//to test the map transfer
	        	map.mapTransfer(map.getMapID()%2+1, 0, 0);
	        	//finishMove();
	        	return false;
	        }
	        

	        initializeMove(dir);

	        //getMap().getTile(x,y).setActor(null);
	        x+= dir.getDx();
	        y+=dir.getDy();
	        //getMap().getTile(x,y).setActor(this);

	        return true;
	    }
	    private void initializeMove(Direction dir){
	    	this.facing=dir;
	        this.srcX = x;
	        this.srcY=y;
	        this.destX=x+dir.getDx();
	        this.destY=y+dir.getDy();
	        this.worldX=x;
	        this.worldY=y;
	        animTimer=0f;
	        state = ACTOR_STATE.WALKING;


	    }
	    private void finishMove(){
	        state=ACTOR_STATE.STANDING;
	        this.worldX = destX;
	        this.worldY = destY;
	        this.srcX=0;
	        this.srcX = 0;
	        this.destX = 0;
	        this.destY=0;
	   }
	    public int getX() {
	        return x;
	    }

	    public int getY() {
	        return y;
	    }

	    public float getWorldX() {
	        return worldX;
	    }

	    public float getWorldY() {
	        return worldY;
	    }

	    public void setX(int x) {
	        this.x = x;
	    }

	    public void setY(int y) {
	        this.y = y;
	    }

	    public void setWorldX(int x) {
	        this.worldX = x;
	    }

	    public void setWorldY(int y) {
	        this.worldY = y;
	    }
		public static TileMap getMap() {
			return map;
		}
		public TextureRegion getSprite () {
			if(state== Actor.ACTOR_STATE.WALKING) {
				return animations.getWalking(facing).getKeyFrame(walkTimer);
			}
				else if (state==ACTOR_STATE.STANDING) {
				return animations.getStanding(facing);
			} else if (state==ACTOR_STATE.REFACING) {
				return animations.getWalking(facing).getKeyFrames()[0];
			}
		return  animations.getStanding(Direction.SOUTH);
}
		public void reface(Direction dir) {
			if(state!=ACTOR_STATE.STANDING) {
				return;
			}
			if (facing == dir) {
				return;
			}
			facing =dir;
			state = ACTOR_STATE.REFACING;
			animTimer = 0f;
		}
		
}
