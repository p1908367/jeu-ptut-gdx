package model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;

import Dialogue.DialogueBox;
import Utile.AnimationSet;
import main.Settings;
import model.Actor.ACTOR_STATE;

public abstract class Entity extends Sprite {
	
	
	protected int x;
	protected int y;
	protected Sprite sprite;
	protected static TileMap map;
	protected float HP;
    protected float worldX,worldY;
    protected int srcX,srcY;
    protected int destX,destY;
    
    protected boolean traverseTout;
    

	//public static TileMap map;

	    //private int x;
	    //private int y;
	    //private float worldX,worldY;
    protected Direction facing;

	    //private int srcX,srcY;
	    //private int destX,destY;
    protected float animTimer;
    protected float ANIM_TIME=0.3f;
    protected float REFACE_TIMER= 0.1f;
    protected ACTOR_STATE state;
    protected float walkTimer;
    protected boolean moveRequestThisFrame;
    protected AnimationSet animations;
    protected DialogueBox box;
    
    protected TextureRegion texture;
	
	public Entity(TileMap map, Sprite sprite, int x, int y, float worldX, float worldY, boolean traverse){
		this.sprite=sprite;
		this.map=map;
		this.y=y;
		this.x=x;
		refreshWorldsPos();
		traverseTout=traverse;
		
        this.state=ACTOR_STATE.STANDING;
        this.facing= Direction.SOUTH;
	}
	
	public Entity(TileMap map, TextureRegion sprite, int x, int y, float worldX, float worldY, boolean traverse){
		this.texture=sprite;
		this.map=map;
		this.y=y;
		this.x=x;
		refreshWorldsPos();
		traverseTout=traverse;
		
        this.state=ACTOR_STATE.STANDING;
        this.facing= Direction.SOUTH;
	}
	
	public Entity(TileMap map, int x, int y, float worldX, float worldY, boolean traverse){
		this.map=map;
		this.y=y;
		this.x=x;
		refreshWorldsPos();
		traverseTout=traverse;
		
        this.state=ACTOR_STATE.STANDING;
        this.facing= Direction.SOUTH;
	}
	
	public void addToMap() {
		System.out.println("added to map at "+ x+", " + y);
        map.getTile(x,y).addEntity(this);
	}
	
	public void delFromMap() {
		map.getTile(x, y).delEntity(this);
	}
	
	public Direction getFacing() {
		return facing;
	}

    public float getWorldX() {
        return worldX;
    }

    public float getWorldY() {
        return worldY;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setWorldX(int x) {
        this.worldX = x;
    }

    public void setWorldY(int y) {
        this.worldY = y;
    }
	public static TileMap getMap() {
		return map;
	}
	
	
	public float getPosX() {
		return x;
	}
	public float getPosY() {
		return y;
	}
	
	public boolean isTraversable() {
		return traverseTout;
	}
	
	public void refreshWorldsPos() {
		worldX=x*Settings.SCALED_TILE_SIZE;
		worldY=y*Settings.SCALED_TILE_SIZE;
		worldX=x;
		worldY=y;
	}
	

    public void update(float delta){
    	//System.out.println("update entity");
        if(state==ACTOR_STATE.WALKING){
            animTimer+=delta;
            walkTimer+=delta;
            worldX= Interpolation.linear.apply(srcX,destX,animTimer/ANIM_TIME);
            worldY=Interpolation.linear.apply(srcY,destY,animTimer/ANIM_TIME);
            if(animTimer>ANIM_TIME){
            	float leftOvertime =animTimer-ANIM_TIME;
            	walkTimer-=leftOvertime;
                finishMove();
                if(moveRequestThisFrame) {
                	move(facing) ;
    	            	
                	
                }else {
                	walkTimer=0f;
                }
            }
        }	
        if (state== ACTOR_STATE.REFACING) {
        	animTimer+=delta;
        	if (animTimer >REFACE_TIMER) {
        		state=ACTOR_STATE.STANDING;
        	}
        }
        moveRequestThisFrame=false;
    }
	
	//public void move(int coordX, int coordY) {
	public boolean move(Direction dir) {
	    if (state==ACTOR_STATE.WALKING){
	    	if (facing ==dir) {
	    		moveRequestThisFrame =true;
	    	}
	    	//System.out.println("bouge d�j�");
	    	return false;
	    }
	    else
	    	this.facing=dir;//so that the he faces the direction even if it is not passable
	    if (x+dir.getDx()>=map.getWidth() || x+dir.getDx()<0 || y+dir.getDy() >=map.getHeight() || y+dir.getDy() <0){
	    	//System.out.println("out");
	        return false;
	    }
	    
	    /*if (map.getTile(x+dir.getDx(),y+dir.getDy()).getActor()!=null){
	        return false;
	    }*/
	    System.out.println("tile at: "+(x+dir.getDx())+", "+(y+dir.getDy()));
	    if (map.getTile(x+dir.getDx(),y+dir.getDy()).passable()==false){
	    	//System.out.println("pas passable");
	        return false;
	    }
	    if(x+dir.getDx()>=80) {//to test the map transfer
	    	map.mapTransfer(map.getMapID()%2+1, 0, 0);
	    	//finishMove();
	    	//System.out.println("transfert");
	    	return false;
	    }
	    
	
	    initializeMove(dir);
	
	    map.getTile(x,y).delEntity(this);
	    x+= dir.getDx();
	    y+=dir.getDy();
	    map.getTile(x,y).addEntity(this);
    	//System.out.println("moving");
	
	    return true;
	}

    private void initializeMove(Direction dir){
    	this.facing=dir;
        this.srcX = x;
        this.srcY=y;
        this.destX=x+dir.getDx();
        this.destY=y+dir.getDy();
        this.worldX=x;
        this.worldY=y;
        animTimer=0f;
        state = ACTOR_STATE.WALKING;


    }

    private void finishMove(){
    	//System.out.println("finished moving");
        state=ACTOR_STATE.STANDING;
        this.worldX = destX;
        this.worldY = destY;
        this.srcX=0;
        this.srcX = 0;
        this.destX = 0;
        this.destY=0;
   }
	
	public void draw(SpriteBatch batch, float x, float y, float xSize, float ySize) {
		if (sprite!=null) {
			batch.draw(sprite, x+worldX*Settings.SCALED_TILE_SIZE, y+worldY*Settings.SCALED_TILE_SIZE, xSize, ySize);
			//System.out.println("drawn at: "+x+worldX*Settings.SCALED_TILE_SIZE+", "+y+worldY*Settings.SCALED_TILE_SIZE);
		}
		if (texture!=null) {
			batch.draw(texture, x+worldX*Settings.SCALED_TILE_SIZE, y+worldY*Settings.SCALED_TILE_SIZE, xSize, ySize);
			//System.out.println("drawn at: "+x+worldX*Settings.SCALED_TILE_SIZE+", "+y+worldY*Settings.SCALED_TILE_SIZE);
		}
	}

	public void reface(Direction dir) {
		if(state!=ACTOR_STATE.STANDING) {
			return;
		}
		if (facing == dir) {
			return;
		}
		facing =dir;
		state = ACTOR_STATE.REFACING;
		animTimer = 0f;
	}
	

	public void executeAction() {
		
	}
	
	
}
