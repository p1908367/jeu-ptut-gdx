package model;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;

import Utile.AnimationSet;
import main.Settings;
import model.Actor.ACTOR_STATE;

public class Player extends Entity {
	
		private float animTimer;
    	private float ANIM_TIME=0.5f;
    	//private ACTOR_STATE state;
        private int srcX,srcY;
	    private AnimationSet animations;
	
		public Player(TileMap map, int x, int y, AnimationSet animations, float worldX, float worldY) {
		super(map, null, x, y, worldX, worldY, false);
		this.animations=animations;
		// TODO Auto-generated constructor stub
		
	}

		// TODO Auto-generated constructor stub


	/*@Override
	public void move(int coordX, int coordY) {
		// TODO Auto-generated method stub
		
	}*/
	
	//method de ilyes import� directement : le move.
	/*
    public void update(float delta){
        if(state==ACTOR_STATE.WALKING){
            animTimer+=delta;
            worldX= Interpolation.linear.apply(srcX,destX,animTimer/ANIM_TIME);
            worldY=Interpolation.linear.apply(srcY,destY,animTimer/ANIM_TIME);
            if(animTimer>ANIM_TIME){
                finishMove();
            }
        }
    }*/
    private void finishMove(){
    state=ACTOR_STATE.STANDING;
    }
    
    private void initializeMove(int oldX, int oldY,int dx,int dy){
        this.srcX = oldX;
        this.srcY=oldY;
        this.destX=oldX+dx;
        this.destY=oldY+dy;
        this.worldX=oldX;
        this.worldY=oldY;
        animTimer=0f;
        state = ACTOR_STATE.WALKING;

    }

	
	public void draw(SpriteBatch batch, float x, float y, float xSize, float ySize) {
		//if (sprite!=null)
		//System.out.println("player drawn at: "+x+worldX*Settings.SCALED_TILE_SIZE+", "+y+worldY*Settings.SCALED_TILE_SIZE);
		batch.draw(getSprite(), worldX*Settings.SCALED_TILE_SIZE+x, worldY*Settings.SCALED_TILE_SIZE+y, xSize, ySize*1.5f);
	}
	public TextureRegion getSprite () {
		if(state== Actor.ACTOR_STATE.WALKING) {
			//System.out.println("got walking: "+facing+", "+walkTimer);
			return animations.getWalking(facing).getKeyFrame(walkTimer);
		} else if (state==ACTOR_STATE.STANDING) {
			//System.out.println("got standing");
			return animations.getStanding(facing);
		} else if (state==ACTOR_STATE.REFACING) {
			//System.out.println("got refacing");
			return animations.getWalking(facing).getKeyFrames()[0];
		}
		//System.out.println("got nothing: "+state);
		return  animations.getStanding(Direction.SOUTH);
	}
}
