package model;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import Dialogue.Dialogue;
import Dialogue.DialogueNode;
import Utile.AnimationSet;
import main.Hacker;
import screen.GameScreen;

public class Pnj extends Entity {
	String actions;
	Dialogue dialogue=null;
	DialogueNode lastNode=null;
	int dialogueId=0;
	
	public Pnj(TileMap map, Sprite sprite, int x, int y, float worldX, float worldY, boolean traverse, String actions) {
		super(map, sprite, x, y, worldX, worldY, traverse);
		this.actions=actions;
	
	}
	
	public Pnj(TileMap map, TextureRegion sprite, int x, int y, float worldX, float worldY, boolean traverse, String actions) {
		super(map, sprite, x, y, worldX, worldY, traverse);
		this.actions=actions;
	
	}
	
	public Pnj(TileMap map, int x, int y, float worldX, float worldY, boolean traverse, String actions) {
		super(map, x, y, worldX, worldY, traverse);
		this.actions=actions;
	}
	
	public void executeAction() {
		String[] actList=actions.split("\n");
		ArrayList<String> multilineAction=new ArrayList<String>();
		boolean multiline=false;
		for(String act: actList) {
			System.out.println("ligne en cours: "+act);
			if(multiline) {
				if(act.strip().equals("}")){
					multilineInterpreter(multilineAction);
					multiline=false;
				}
				multilineAction.add(act);
			}
			multiline=lineInterpreter(act);
			if(multiline) {
				multilineAction=new ArrayList<String>();
				multilineAction.add(act);
			}
		}
		if(dialogue!=null) {
	        GameScreen.getDialogueController().startDialogue(dialogue);
			
		}
		dialogue=null;
		dialogueId=0;
	}
	
	public String deleteCommand(String action) {
		boolean ok=true;
		while(ok) {
			if(action.charAt(0)=='(') {
				ok=false;
			}
			action=action.substring(1);
		}
		ok=true;
		while(ok) {
			int n=action.length();
			if(action.charAt(n-1)==')') {
				ok=false;
			}
			action=action.substring(0, n-1);
		}
		return action;
	}
	
	public boolean lineInterpreter(String action) {
		if(action.startsWith("msg(")) {//demande d'afficher un message: msg(texte)
			String message=deleteCommand(action);
			
			if(dialogue==null) {
				dialogue=new Dialogue();
			}
	        DialogueNode node= new DialogueNode(message,dialogueId);
	        
	        if(lastNode!=null) {
	        	lastNode.makeLinear(node.getID());
	        }
	        
	        dialogue.addNode(node);
	        dialogueId++;
	        lastNode=node;
		} else if(action.startsWith("money(")) {//augmente l'argent: money(val)
			String arg=deleteCommand(action);
			int mon=Integer.parseInt(arg);
			this.map.screen.valArgent+=mon;
		} else if(action.startsWith("tp(")) { //teleporte le joueur: tp(mapId, x, y)
			String arg=deleteCommand(action);
			String args[]=arg.split(",");
			int id=Integer.parseInt(args[0].trim());
			int x=Integer.parseInt(args[1].trim());
			int y=Integer.parseInt(args[2].trim());
			this.map.mapTransfer(id, x, y);
		} else if(action.startsWith("minigame(")){
			String arg=deleteCommand(action);
			int gameId=Integer.parseInt(arg);
			Hacker.launchMiniGame(gameId);
		} else if(action.startsWith("shop(")) {
			Hacker.launchShop();
		} else {
			System.out.println("ligne inconnue et ignor�e...");
		}
		return false;
	}
	
	public void multilineInterpreter(ArrayList<String> action) {
		
	}
	

}
