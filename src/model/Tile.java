package model;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import main.Settings;

public class Tile {
    private TERRAIN terrain;
    //private Actor actor;
    private ArrayList<Entity> entities;
    private TextureRegion texture;//ground texture (1st layout)
    private TextureRegion texture2;//2nd layout texture
    private TextureRegion texture3;//3rd layout texture
    public boolean textureOver;

/*
    public Actor getActor() {
        return actor;
    }
    public void setActor(Actor actor) {
        this.actor = actor;
    }*/


    public Entity getEntity(int i) {
        return entities.get(i);
    }
    public void addEntity(Entity ent) {
        this.entities.add(ent);
    }
    public void delEntity(Entity ent) {
    	this.entities.remove(ent);
    }


    public Tile(TERRAIN terrain) {
        this.terrain = terrain;
        textureOver=false;
        entities = new ArrayList<Entity>();
    }

    public TERRAIN getTerrain() {
        return terrain;
    }
    
    public void setTexture(Texture tileset, int place) {
    	int x, y, size=Settings.TILE_SIZE;
    	x=place%(tileset.getWidth()/size);
    	y=place/(tileset.getWidth()/size);
    	System.out.println(place+": set texture at "+x+", "+y);
    	texture=new TextureRegion(tileset, x*size, y*size, size, size);
    }
    
    public void setTexture2(Texture tileset, int place) {
    	int x, y, size=Settings.TILE_SIZE;
    	x=place%(tileset.getWidth()/size);
    	y=place/(tileset.getWidth()/size);
    	System.out.println(place+": set texture at "+x+", "+y);
    	texture2=new TextureRegion(tileset, x*size, y*size, size, size);
    }
    
    public void setTexture3(Texture tileset, int place) {
    	int x, y, size=Settings.TILE_SIZE;
    	x=place%(tileset.getWidth()/size);
    	y=place/(tileset.getWidth()/size);
    	System.out.println(place+": set texture at "+x+", "+y);
    	texture3=new TextureRegion(tileset, x*size, y*size, size, size);
    }
    
    public TextureRegion getTexture() {
    	return texture;
    }
    
    public TextureRegion getTexture2() {
    	return texture2;
    }
    
    public TextureRegion getTexture3() {
    	return texture3;
    }
    public String toString() {
    	return "tile: "+terrain+"   "+texture+"   "+texture2+"   "+texture3;
    }
    public boolean passable() {
    	if(terrain==TERRAIN.GRASS_1) {
    		return false;
    	}
    	boolean passable=true;
    	for(Entity ent: entities) {
    		passable=passable&&ent.isTraversable();
    		System.out.println("entity: "+ent.isTraversable());
    	}
    	return passable;
    }
    public void drawUnder(SpriteBatch batch, float x, float y, float xSize, float ySize) {//draw the textures that are under the player (ground texture and normal texture if not over)
        if(texture!=null)
        	batch.draw(texture, x, y, xSize, ySize);
        if(texture2!=null && textureOver==false)
        	batch.draw(texture2, x, y, xSize, ySize);
        /*for(Entity ent: entities) {
        	ent.draw(batch, x, y, xSize, ySize);
        	//System.out.println("drawn entity");
        }*/
    }
    
    public void drawEntities(SpriteBatch batch, float x, float y, float xSize, float ySize) {
    	//System.out.println("drawing entities");
        for(Entity ent: entities) {
        	//System.out.println("entity found");
        	ent.draw(batch, x, y, xSize, ySize);
        }
    }
    
    public void drawOver(SpriteBatch batch, float x, float y, float xSize, float ySize) {//draw the textures that are over the player (normal texture if over)
        if(texture2!=null && textureOver)
        	batch.draw(texture2, x, y, xSize, ySize);
        if(texture3!=null)
        	batch.draw(texture3, x, y, xSize, ySize);
    	
    }
    
    public void executeEntities() {
    	for(Entity ent: entities) {
    		ent.executeAction();
    	}
    }
}
