package model;

public class TileBis {
    private TERRAIN terrain;
    private Entity entity;


    public Entity getActor() {
        return entity;
    }
    public void setEntity(Entity entity) {
        this.entity = entity;
    }


    public TileBis(TERRAIN terrain) {
        this.terrain = terrain;
    }

    public TERRAIN getTerrain() {
        return terrain;
    }
}
