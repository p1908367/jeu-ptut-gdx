package model;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import main.Hacker;
import screen.GameScreen;
import screen.ScreenManager;

/*import com.Block;
import com.ID;
import com.Player;*/

public class TileMap {
    private int width, height, id;
    private Tile [][] tiles;
    public Texture groundTileset;
    public Texture tileset;
    public GameScreen screen;

    public TileMap( int width,int height, String groundPath, String tilesetPath, GameScreen screen){
        groundTileset = new Texture("assets/graphics/tilesets/"+groundPath);
        tileset = new Texture("assets/graphics/tilesets/"+tilesetPath);
    	this.id=0;
        this.height=height;
        this.width=width;
        this.screen=screen;


        /*tiles= new Tile[width][height];
        for (int x=0; x<width;x++){
            for (int y=0;y<height;y++){
                if (Math.random()>0.5d ){
                    tiles [x][y]=new Tile(TERRAIN.GRASS_1);
                }else
                tiles [x][y]=new Tile(TERRAIN.GRASS_2);
            }
        }*/
        changeMapSize(1, 1);
        //mapTransfer(1, 1, 1);//id, player's x position, player's y position

    }
    public Tile getTile (int x, int y){
        return tiles[y][x];
    }
    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }
    
    
    public void changeMapSize(int x, int y) {
    	width=x;
    	height=y;
    	tiles= new Tile[height][width];
        for (int xx=0; xx<width;xx++){
            for (int yy=0;yy<height;yy++){
                tiles [yy][xx]=new Tile(TERRAIN.GRASS_2);
                tiles[yy][xx].setTexture(groundTileset, 1);
            }
        }
        System.out.println("changement de taille de la map: "+width+"x"+height);
    }
    public void changeTilesets(String grTset, String tset) {
        groundTileset = new Texture("assets/graphics/tilesets/"+grTset);
        tileset = new Texture("assets/graphics/tilesets/"+tset);
    }
    

    public void loadMap(int id) /*throws Exception*/ {
		//handler=new Handler(this);
        File map = new File("files/maps/map"+id+".txt");
        //File map = new File("map1.txt");
    	//System.out.println(arguments[0]+"map1");
		//File map = new File(arguments[0]+"map1");
        //File map = new File(arguments[0]+"map"+id+".txt");
        String mapInfo="";
        if (map.isFile() && map.canRead() ) {
        	System.out.println("la map est utilisable");
        	try {
        	FileReader rd = new FileReader(map);
    		int ch;
    		while((ch=rd.read())!=-1) {
    			mapInfo+=(char)ch;
    		}
    		rd.close();
    		mapInfo=mapInfo.trim();//deletes the useless spaces at the beginning and the end of the string
    		//System.out.println(mapInfo);
        	}
        	catch(Exception probleme) {
        		System.out.println("Une erreur est survenue lors du chargement de la map");
        		//erreur=true;
        		System.exit(0);
        	}
        }
        else {
        	System.out.println("erreur, la map n'est pas utilisable ou est introuvable");
        	//erreur=true;
        	System.exit(0);
        }
    	/*handler.object.clear();
        handler.addObject(new Player( 10, 10, ID.Player, 32, 32, handler, this));
        handler.addObject(new Block(300, 200, ID.Talbe, 32, 32));
        handler.addObject(new Block(500,200, ID.Tableau, 32, 32));*/
        
        
    	//if (id==1) {
        //handler.addObject(new Player(100, 100, ID.Player, handler));
    	/*if (id==1) {
            handler.addObject(new Block(Main.WIDTH / 2, Main.HEIGHT / 2, ID.Enemy));
            handler.addObject(new Block(100, 55, ID.Enemy));
            handler.addObject(new Block(300, 160, ID.Talbe));
    	}
    	else {
            handler.addObject(new Block(200, 200, ID.Talbe));
    	}*/
        createMap(mapInfo);
        screen.addEntitiesToMap();
    }
    

    public void createMap(String mapinfos) {
    	System.out.println("infos de la map:\n"+mapinfos+"\n\n");
    	String actualLine="";//will contain each line of the map (each line of the file)
    	int actualChar=0, /*lineStart=0,*/ len=mapinfos.length();//contains the index of the start of the line and the actual character
    	boolean multiLine=false;
    	while (actualChar<len) {
    		if(mapinfos.charAt(actualChar)=='\n' || actualChar==len-1) {//end of line
    			if(actualChar==len-1)
    				actualLine+=mapinfos.charAt(actualChar);
    			System.out.println("ligne en cours: "+actualLine);
    			multiLine=lineInterpreter(actualLine);
    			if(multiLine) {
    				actualChar+=1;
    				while(actualChar<len && mapinfos.charAt(actualChar)!='}') {
    					actualLine+=mapinfos.charAt(actualChar);
    					actualChar+=1;
    				}
    				multiLineInterpreter(actualLine);
    				multiLine=false;
    			}
    			actualLine="";
    		}
    		/*else if(actualChar==len-1) {
    			actualLine+=mapinfos.charAt(actualChar);
    			System.out.println(actualLine);
    			multiLine=lineInterpreter(actualLine);
    			actualLine="";
    		}*/
    		else
    			actualLine+=mapinfos.charAt(actualChar);
    		actualChar+=1;
    	}
    	/*for(int i=0; i<tiles.length; i++) {
    		for(int j=0; j<tiles[i].length; j++) {
    			System.out.println(tiles[i][j]);
    		}
    	}*/
    	
    }
    
    public boolean lineInterpreter(String line) {/**interprets the line, creating an element of the map.
    * if the line is misspelled or unknown, does nothing
    * 
    * Rules: -example: ElementName(parameter1; parameter 2, ...)
    * 	-parameters are split by a ';'
    * 	-spaces in parameters that are not strings are ignored
    * 	-a line MUST be finished by a ')' to work properly! the line wont be used otherwise. (everything after the ')' will be ignored)
    */
    	String[] parametres;
    	boolean needMore=false;
    	//System.out.println("interprete la ligne : "+line);
    	
    	line=getUsefulLineContent(line);
    	
    	//now, will check what is the element wanted
    	if(line.startsWith("bloc(")) {//make a block. 2 arguments are asked. the arguments asked are position x and position y
    		//System.out.println("faire un bloc");
    		//line=line.replaceFirst("bloc(",  "");//delete the "bloc(" instruction
    		line=line.substring(5,  line.length());//delete the "bloc(" instruction
    		line=line.replace(" ", "");//delete all the spaces (because they are useless)
    		//System.out.println("arguments: "+ line);
			parametres=line.split(";");
        	if (parametres.length==2) {//2 parameters wanted
        		try {//try to turn the arguments into integers
        			int param1, param2;
        			param1=Integer.parseInt(parametres[0]);
        			param2=Integer.parseInt(parametres[1]);
        			//handler.addObject(new Block(param1, param2, ID.Enemy, 32, 32));
                    tiles [param2][param1]=new Tile(TERRAIN.GRASS_1);//creates a bloc at the wanted position
        			System.out.println("bloc cr��");
        		}
        		catch(Exception probleme) {//if it can't be done, don't do anything
        			System.out.println("arguments invalides");
        		}
    		}
        	else {//not the required number of argument
        		System.out.println("nombre d'arguments invalide");
        	}
    	}
    	else if(line.startsWith("tilesets(")) {
    		line=line.substring(9,  line.length());//delete the "bloc(" instruction
    		line=line.replace(" ", "");//delete all the spaces (because they are useless)

			parametres=line.split(";");
        	if (parametres.length==2) {//2 parameters wanted
        		try {//try to turn the arguments into integers
        			String param1, param2;
        			param1=parametres[0];
        			param2=parametres[1];
        			//width=param1;
        			//height=param2;
        			System.out.println(1);
        			changeTilesets(param1, param2);
        			System.out.println(2);
        			System.out.println("tilesets: "+ param1+" "+param2);
        		}
        		catch(Exception probleme) {//if it can't be done, don't do anything
        			System.out.println("arguments invalides: "+probleme);
        		}
    		}
        	else {//not the required number of argument
        		System.out.println("nombre d'arguments invalide");
        	}
    		
    	}
    	else if(line.startsWith("mapSize(")) {
    		line=line.substring(8,  line.length());//delete the "bloc(" instruction
    		line=line.replace(" ", "");//delete all the spaces (because they are useless)

			parametres=line.split(";");
        	if (parametres.length==2) {//2 parameters wanted
        		try {//try to turn the arguments into integers
        			int param1, param2;
        			param1=Integer.parseInt(parametres[0]);
        			param2=Integer.parseInt(parametres[1]);
        			//width=param1;
        			//height=param2;
        			System.out.println(1);
        			changeMapSize(param1, param2);
        			System.out.println(2);
        			System.out.println("taille de map: "+ param1+" "+param2);
        			System.out.println("taille de map: "+ width+" "+height);
        		}
        		catch(Exception probleme) {//if it can't be done, don't do anything
        			System.out.println("arguments invalides: "+probleme);
        		}
    		}
        	else {//not the required number of argument
        		System.out.println("nombre d'arguments invalide");
        	}
    		
    	}
    	else if(line.startsWith("map(")) {
    		System.out.println("creer map");
    		needMore=true;
    	}
    	else if(line.startsWith("groundTileset(")) {
    		System.out.println("attribution des textures des tiles (sol)");
    		needMore=true;
    	}
    	else if(line.startsWith("normalTileset(")) {
    		System.out.println("attribution des textures de la 2e couche");
    		needMore=true;
    	}
    	else if(line.startsWith("Textures3eC(")) {
    		System.out.println("attribution des textures de la 3e couche");
    		needMore=true;
    	}
    	else if(line.startsWith("overTiles(")) {
    		System.out.println("attribution des textures qui passent au dessus du joueur");
    		needMore=true;
    	}
    	else if(line.startsWith("entity(")) {
    		System.out.println("creation d'une entite");
    		needMore=true;
    	}
    	else {//unknown element
    		System.out.println("ligne ignor�e");
    	}
    	System.out.println("");
    	return needMore;
    }
    
    public void multiLineInterpreter(String lines) {
    	lines=lines.trim();
    	ArrayList<String> line=new ArrayList<String>();
    	for(String l:lines.split("\n")) {
    		//System.out.println(l+"fin");
    		line.add(l);
    	}
    	int len=line.size();
    	if(line.get(0).startsWith("map(") || line.get(0).startsWith("overTiles(")) {//this is the map that is asked to be created or the tiles that are over the player
    		boolean createMap;
    		if(line.get(0).startsWith("map(")) {
    			System.out.println("creation de la map");
    			line.set(0, line.get(0).substring(7));//deletes the command from the first line
    			createMap=true;
    		} else {
    			System.out.println("attribution des tiles au dessus du joueur");
    			line.set(0, line.get(0).substring(13));//deletes the command from the first line
    			createMap=false;
    		}
    		//int height=len;
    		for(int y=0; y<len; y++) {
    			String actualLine=line.get(y).trim();
    			//actualLine.substring(beginIndex)
    			System.out.println("d"+actualLine+"f");
    			int width=actualLine.length();
    			for(int x=0; x<width; x++) {
    				if(actualLine.charAt(x)=='x') {
    					/*int posx, posy, sizex, sizey;
    					posx=x*(this.width/width);
    					posy=y*(this.height/height);
    					sizex=this.width/width;
    					sizey=this.height/height;
            			handler.addObject(new Block(posx, posy, ID.Enemy, sizex, sizey));*/
                        if (createMap) {
                        	System.out.println(height+"-"+x+"="+(height-x));
                        	tiles [height-y-1][x]=new Tile(TERRAIN.GRASS_1);//creates a bloc at the wanted position. the height is inverted because this dumbass of jdx has an inverted y axis!!
                        } else {
                        	//tiles [height-y-1][x]=new Tile(TERRAIN.GRASS_1);
                        	tiles[height-y-1][x].textureOver=true;
                        }
                        //System.out.println("bloc: "+posx+" "+posy+" "+sizex+" "+sizey);
    					
    				}
    			}
    		}
    	}//Textures3eC
    	if(line.get(0).startsWith("groundTileset(") || line.get(0).startsWith("normalTileset(") || line.get(0).startsWith("Textures3eC(")) {//this is the map that is asked to be designed
    		boolean ground, texture3eC=false;
    		if(line.get(0).startsWith("groundTileset(")) {
    			System.out.println("creation des textures du sol de la map");
    			ground=true;
        		line.set(0, line.get(0).substring(17));//deletes the command from the first line
    		} else {
    			ground=false;
    			if(line.get(0).startsWith("normalTileset(")) {
        			System.out.println("creation des textures de la 2e couche");
        			texture3eC=false;
            		line.set(0, line.get(0).substring(17));//deletes the command from the first line
    			} else {
    				System.out.println("creation des textures de la 3e couche");
        			texture3eC=true;
            		line.set(0, line.get(0).substring(15));//deletes the command from the first line
    			}
    		}
    		//int height=len;
    		for(int y=0; y<len; y++) {
    			String actualLine=line.get(y).trim();
    			//actualLine.substring(beginIndex)
    			System.out.println("d"+actualLine+"f");
    			int x=0;
    			for(String arg:actualLine.split(",")){
    				int num=Integer.parseInt(arg.trim());
    				System.out.print("tile "+x+", "+y+": ");
    				if(ground)
    					tiles[height-y-1][x].setTexture(groundTileset, num);
    				else {
    					if (texture3eC) {
    						tiles[height-y-1][x].setTexture3(tileset, num);
    					} else {
    						tiles[height-y-1][x].setTexture2(tileset, num);
    					}
    				}
    				//System.out.println("tile "+x+", "+y+", "+num+", "+tiles[height-y-1][x].getTexture());
    				x++;
    			}
    		}
    	}

    	if(line.get(0).startsWith("entity(")) {
        	line.set(0, line.get(0).substring(10));//deletes the command from the first line

        	int lineCount=0;
        	int x=0, y=0;
        	String path="", act="";
        	boolean trav=false;
        	for(String l: line) {
        		if(lineCount==0) {
                	String[] parametres=l.split(";");
        			x=Integer.parseInt(parametres[0].trim());
        			y=Integer.parseInt(parametres[1].trim());
        			path=parametres[2].trim();
        			trav=stringToBool(parametres[3]);
        		} else {
        			if(lineCount>=2) {
        				act+="\n";
        			}
        			act+=l;
        		}
        		lineCount++;
        	}
        	Pnj ent;
        	y=height-y-1;
        	if(path.equals("none")==false) {//si le chemin du sprite est "none", on ne met pas de sprite (pnj invisible)
        		TextureRegion t = new TextureRegion(new Texture(path));
                ent = new Pnj(this, t, x, y, (float)0.0, (float)0.0, trav, act);
        	} else {
                ent = new Pnj(this, x, y, (float)0.0, (float)0.0, trav, act);
        	}
            screen.addEntity(ent);
    	}
    	
    	
    }
    
    
    
    public String getUsefulLineContent(String line) {/**deletes everything that is not useful to the line
    *(things that are after the parenthesis)
    */
    	while(line.length()>0 && line.charAt(line.length()-1)!=')') {//delete everything that is not between the parenthesis (except the element asked)
    		line=line.substring(0,  line.length()-1);
    		//System.out.println("suppr");
    	}
    	if(line.length()>0 && line.charAt(line.length()-1)==')'){//delete the ")" at the end
    		line=line.substring(0,  line.length()-1);
    		//System.out.println("suppr");
    	}
    	
    	return line;
    }
    
    /*public boolean paramNumberOk(int numparam, String line) {//return true if the number of parameter is the one asked (numparam)		USELESS
    	//int param=0;
    	//if (line.split(",").length()==numparam)
    	return (line.split(",").length==numparam);
    }*/
    
    public void beginTranser(Entity p) {
    	loadMap(1);
    	this.id=1;
    	p.setX(0);
    	p.setY(0);
    }
    
    public void mapTransfer(int id, int x, int y) {//lead the player to the map at the position in argument
    	screen.setPlayerPosition(x, y);
    	if (id!=this.id){//do not load the map anew if the wanted map is the actual map
    		this.screen.delEntities();
    		loadMap(id);
    	}
    	this.id=id;
    	//placePlayer(x, y);
    	//Actor player=ScreenManager.getGameScreen().player;
    	/*handler.object.get(0).setX(x);//set the player's position
    	handler.object.get(0).setY(y);
    	camera.setX(handler.object.get(0).getX());
    	camera.setY(handler.object.get(0).getY());*/
    }
    public void placePlayer(int x, int y) {
    	/*Actor player=Hacker.gsm.getGameScreen().player;
    	if(player!=null) {
    		player.setX(x);
    		player.setY(y);
    	}*/
    	//Hacker.gsm.getGameScreen().setPlayer(x, y);
    	//screen.getPlayer().delFromMap();
    	screen.setPlayer(x, y, true);
    	//screen.getPlayer().addToMap();
    }
    
    public int getMapID() {return id;}
    
    public boolean isInMap(int x, int y) {
    	boolean inMap=false;
    	if(x>=0 && x<width) {
    		if(y>=0 && y<height) {
    			inMap=true;
    		}
    	}
    	return inMap;
    }
    
    public boolean stringToBool(String s) {
        /* *converts the string to a boolean. return False otherwhise.
        * correct booleans are
        * -True: "t", "true", "True", "y", "yes"
        */
        boolean isBoolean=false;
        s=s.trim();
        if (s.equals("t") || s.equals("true") || s.equals("True") || s.equals("y") || s.equals("yes")){
            isBoolean=true;
        }
        return isBoolean;
    }
}
