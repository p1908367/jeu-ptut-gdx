package save;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

import Controller.PlayerController;
import model.Actor;
import screen.GameScreen;

public class Save {
	
	public static ArrayList<Integer> saveFileInfo = new ArrayList<Integer>();
	
	public static void saveGame(){
	    try {
	      File Save = new File("files/Save/Save1.txt");                          // On cherche "Save 1.txt" dans le fichier des sauvegardes
	      if (Save.createNewFile()) {											 // S'il n'�xiste pas, on le cr�e
	        System.out.println("Fichier de sauvegarde cr�e : " + Save.getName());
	      } else {
	        System.out.println("Fichier de sauvegarde d�j� existant.");			// Sinon on affiche un message indiquant qu'il a �t� trouv�
	      }
	      
	      FileWriter saveUpdate = new FileWriter("files/Save/Save1.txt");		//On cr�e un FileWriter pour mettre � jour le fichier
	      saveUpdate.write(/*"PlayerX : "*/(int)screen.GameScreen.getPlayer().getWorldX()+"\n");	// |
	      saveUpdate.write(/*"PlayerY : "+*/(int)screen.GameScreen.getPlayer().getWorldY()+"\n");   // |  On �crit les diff�rentes variables
	      saveUpdate.write(model.Player.getMap().getMapID()+"\n");									// |  que l'on veut stocker dans dans 
	      saveUpdate.write(screen.GameScreen.valArgent+"\n");  //Save player Money					// |  le fichier yexye
	      saveUpdate.write(screen.GameScreen.controller.getLvl()+"\n");     						// |
	      
	      saveUpdate.close();	// On ferme le FileWriter 
	      System.out.println("Partie sauvegard�e"); // On affiche un message de r�ussite
	    } catch (IOException e) {
	      System.out.println("Erreur");             // On affiche un message en cas d'erreur
	      e.printStackTrace();
	    }    
	    
	  }

	
	public static void loadSave() /*throws Exception*/ {		
		try{
			saveFileInfo.clear();									 // On vide le tableau saveFileInfo
			File Save = new File("files/Save/Save1.txt");            // On cr�e une File afin d'y stocker le fichier de sauvegarde
			InputStream ips=new FileInputStream(Save);               // On cr�e un Stream afin de lire le fichier en param�tre
			InputStreamReader ipsr=new InputStreamReader(ips);       //
			BufferedReader br=new BufferedReader(ipsr);
			String ligne = null;					       // On cr�e une variable sting qui contiendra la valeur de la ligne lu
			while ((ligne=br.readLine())!=null){		   // On parcours chaque ligne du fichier et pour chaque ligne on enregistre 
														   // dans une des cases du tableau  
				saveFileInfo.add(Integer.parseInt(ligne)); // On converti les strings du fichier en Int
			}
			screen.GameScreen.setPlayer( saveFileInfo.get(0), saveFileInfo.get(1),true);  //  |
			System.out.println("Map : "+saveFileInfo.get(2));                             //  |
			System.out.println("X : "+saveFileInfo.get(0));                               //  | On affecte aux diff�rentes variables
			System.out.println("Y : "+saveFileInfo.get(1));                               //  | leurs valeur enregist�s
			screen.GameScreen.valArgent = saveFileInfo.get(3);		                      //  |
			screen.GameScreen.controller.lvl = saveFileInfo.get(4);                       //  |
			System.out.println("Sauvegarde charg�");          // On affiche un message de r�ussite
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());      // On affiche un message en cas d'erreur
		}
		
	}

	
}
