package screen;

import com.badlogic.gdx.Screen;
import main.Hacker;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import main.Hacker;

public abstract class AbstractScreen implements Screen {
    protected final Hacker app;

    Stage stage;
    public AbstractScreen(final Hacker app) {
        this.app = app;
        this.stage=new Stage();

    }

    @Override
    public abstract void hide();

    @Override
    public abstract void pause();

    public abstract void update(float delta);

    @Override
    public  void render(float delta){
    	
        update(delta);
        Gdx.gl.glClearColor(1f,1f,1f,1f);  //c'est pour effacer l'ecran entre 2 ecran
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);               //c'est pour effacer l'ecran entre 2 ecran

    }

    @Override
    public void  resize(int width, int height){
    stage.getViewport().update(width,height,true);
    }

    @Override
    public abstract void resume();

    @Override
    public abstract void show();

    @Override
    public  void dispose() {
    stage.dispose();
    }
    public Hacker getApp() {
    	return app;
    }
}
