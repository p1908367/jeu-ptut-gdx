package screen;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import main.Hacker;
import screen.ScreenManager.STATE;

public class AnimationCases extends AbstractScreen implements ApplicationListener, InputProcessor{

	public static Stage animStage;
    Skin skin;
    public Batch batch;
    public TextureRegion backgroundTexture;
    TextureAtlas atlas;
    Array<AtlasRegion> animationframe;
    Animation anim;
    float timer;
    float frameanimationtime=2.5f;
    private String[] images= {"tab1","tab2","tab3","tab4","tab5"};
    static int oui=(int)(Math.random() * ((4) + 1));
	
	public AnimationCases(Hacker app) {
		super(app);
		animStage = new Stage();
        batch = new SpriteBatch();
		backgroundTexture = new TextureRegion(new Texture("assets/Cases/background.jpg"));
		createAnimSkin();
	
        TextButton jouer = new TextButton("Jouer", skin);
        jouer.setPosition(590,10);
        jouer.setSize(100,30);
        jouer.setVisible(true);
        jouer.addListener(new ClickListener() {


            @Override
            public void clicked(InputEvent event, float x, float y) {
            	
            	Hacker.gsm.setScreens(STATE.Cases);
            	Gdx.input.setInputProcessor(CasesScreen.casesStage);
            	
                }

        });
        
        animStage.addActor(jouer);
		
        TextButton retour = new TextButton("Retour", skin);
        retour.setPosition(128,10);
        retour.setSize(100,30);
        retour.setVisible(true);
        retour.addListener(new ClickListener() {


            @Override
            public void clicked(InputEvent event, float x, float y) {
            	
            	Hacker.gsm.setScreens(STATE.PLAY);
                }

        });
        
        animStage.addActor(retour);
		
		
	}

	
	public void createAnimSkin(){
        BitmapFont font = new BitmapFont();
        skin= new Skin();
        skin.add("default",font);

        Pixmap pixmap= new Pixmap((int) Gdx.graphics.getWidth()/4,(int)Gdx.graphics.getHeight()/10, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background", new Texture(pixmap));

        TextButton.TextButtonStyle  textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up= skin.newDrawable("background",Color.BLACK);
        textButtonStyle.down= skin.newDrawable("background",Color.DARK_GRAY);
        textButtonStyle.over= skin.newDrawable("background",Color.LIGHT_GRAY);
        textButtonStyle.font= skin.getFont("default");
        skin.add("default",textButtonStyle);

    }
	
	
	
	@Override
	public void render(float delta) {
        super.render(delta);
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        timer += Gdx.graphics.getDeltaTime();
        
		batch.begin();
		animStage.act();
		batch.draw(backgroundTexture,0,0,1280,720);
		batch.draw(anim.getKeyFrame(timer),341,60);
		batch.end();
		animStage.draw();
		
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		atlas=new TextureAtlas(Gdx.files.internal("assets/Cases/"+images[oui]+"/lol.pack"));
		animationframe=atlas.getRegions();
		
		anim = new Animation(frameanimationtime,animationframe);
		
		anim.setPlayMode(Animation.PlayMode.NORMAL);
		
		timer=0.0f;
					
		System.out.println("Nbre Frames : "+animationframe.size);
		System.out.println("dur�e Totale : "+anim.getAnimationDuration());
		System.out.println("Dur�e d'une Frame :"+anim.getFrameDuration());
		System.out.println("play Mode : "+anim.getPlayMode());
	}

	public static int getTab() {
		return oui;
	}
	
	public static void changeTab() {
		oui=(int)(Math.random() * ((4) + 1));
	}
	
	@Override
	public boolean keyDown(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		
	}

}
