package screen;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import Controller.PlayerController;
import main.Hacker;
import screen.ScreenManager.STATE;

public class CasesScreen extends AbstractScreen implements ApplicationListener, InputProcessor {

	public static Stage casesStage;
	Skin skin;
	public Batch batch;
	public TextureRegion backgroundTexture;
	TextureRegion card1, card2, card3, card4, card5, card6, card7, card8, card9, card10;
	int a = 199;
	int d = 200;
	int b = 340;
	int c = 60;
	int test[][] = { { 8, 6, 4, 2, 9 }, { 1, 8, 4, 5, 1 }, { 5, 3, 8, 6, 7 }, { 2, 9, 4, 7, 4 }, { 5, 6, 9, 4, 3 } };
	int z = 0;
	int tab[] = new int[5];

	public CasesScreen(final Hacker app) {
		super(app);
		casesStage = new Stage();
		batch = new SpriteBatch();
		backgroundTexture = new TextureRegion(new Texture("assets/Cases/background.jpg"));
		createCasesSkin();
		initialisation();
		initBoutton();

	}

	public void initialisation() {
		card1 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/1.jpg")));
		card2 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/1.jpg")));
		card3 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/1.jpg")));
		card4 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/1.jpg")));
		card5 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/1.jpg")));
		card6 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/1.jpg")));
		card7 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/1.jpg")));
		card8 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/1.jpg")));
		card9 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/1.jpg")));
		int info = screen.AnimationCases.getTab();
		for (int i = 0; i < 5; i++) {
			tab[i] = test[info][i];
		}
	}

	public void createCasesSkin() {
		BitmapFont font = new BitmapFont();
		skin = new Skin();
		skin.add("default", font);

		Pixmap pixmap = new Pixmap((int) Gdx.graphics.getWidth() / 4, (int) Gdx.graphics.getHeight() / 10,
				Pixmap.Format.RGB888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("background", new Texture(pixmap));

		TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
		textButtonStyle.up = skin.newDrawable("background", Color.RED);
		textButtonStyle.down = skin.newDrawable("background", Color.GREEN);
		textButtonStyle.over = skin.newDrawable("background", Color.GREEN);
		textButtonStyle.font = skin.getFont("default");
		skin.add("default", textButtonStyle);

	}

	@Override
	public void render(float delta) {
		super.render(delta);

		batch.begin();
		casesStage.act();
		batch.draw(backgroundTexture, 0, 0, 1280, 720);
		batch.draw(card1, b, c, a, d);
		batch.draw(card2, a + b, c, a, d);
		batch.draw(card3, a * 2 + b, c, a, d);
		batch.draw(card4, b, a + c, a, d);
		batch.draw(card5, a + b, a + c, a, d);
		batch.draw(card6, a * 2 + b, a + c, a, d);
		batch.draw(card7, 0 + b, a * 2 + c, a, d);
		batch.draw(card8, a + b, a * 2 + c, a, d);
		batch.draw(card9, a * 2 + b, a * 2 + c, a, d);
		batch.end();
		casesStage.draw();

	}

	public void finishcases() {
		if (PlayerController.healthBar > 4) {
			PlayerController.healthBar = 0;
			PlayerController.lvl++;
		} else {
			PlayerController.healthBar++;
		}
		GameScreen.valArgent += 250;
		screen.AnimationCases.changeTab();
	}

	public void initBoutton() {
		TextButton revoir = new TextButton("Revoir", skin);
		revoir.setPosition(590, 10);
		revoir.setSize(100, 30);
		revoir.setVisible(true);
		revoir.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {

				initialisation();
				z = 0;
				Hacker.gsm.setScreens(STATE.Anim);
				Gdx.input.setInputProcessor(AnimationCases.animStage);
			}

		});

		casesStage.addActor(revoir);

		TextButton quitter = new TextButton("Retour", skin);
		quitter.setPosition(128, 10);
		quitter.setSize(100, 30);
		quitter.setVisible(true);
		quitter.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				initialisation();
				z = 0;
				Hacker.gsm.setScreens(STATE.PLAY);
			}

		});

		casesStage.addActor(quitter);

		TextButton un = new TextButton("", skin);
		un.setPosition(b + a / 2 - 15, c + d / 2 - 15);
		un.setSize(30, 30);
		un.setVisible(true);
		un.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (tab[z] == 1) {
					card1 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/2.jpg")));
					z++;
					if (z == 5) {
						initialisation();
						z = 0;
						Hacker.gsm.setScreens(STATE.PLAY);
						finishcases();
					}
				} else {
					initialisation();
	            	z=0;
	            	Hacker.gsm.setScreens(STATE.Anim);
	            	Gdx.input.setInputProcessor(AnimationCases.animStage);
				}
			}

		});

		casesStage.addActor(un);

		TextButton deux = new TextButton("", skin);
		deux.setPosition(a + b + a / 2 - 15, c + d / 2 - 15);
		deux.setSize(30, 30);
		deux.setVisible(true);
		deux.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (tab[z] == 2) {
					card2 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/2.jpg")));
					z++;
					if (z == 5) {
						initialisation();
						z = 0;
						Hacker.gsm.setScreens(STATE.PLAY);
						finishcases();
					}
				} else {
					initialisation();
	            	z=0;
	            	Hacker.gsm.setScreens(STATE.Anim);
	            	Gdx.input.setInputProcessor(AnimationCases.animStage);
				}
			}

		});

		casesStage.addActor(deux);

		TextButton trois = new TextButton("", skin);
		trois.setPosition(a * 2 + b + a / 2 - 15, c + d / 2 - 15);
		trois.setSize(30, 30);
		trois.setVisible(true);
		trois.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (tab[z] == 3) {
					card3 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/2.jpg")));
					z++;
					if (z == 5) {
						initialisation();
						z = 0;
						Hacker.gsm.setScreens(STATE.PLAY);
						finishcases();
					}
				} else {
					initialisation();
	            	z=0;
	            	Hacker.gsm.setScreens(STATE.Anim);
	            	Gdx.input.setInputProcessor(AnimationCases.animStage);
				}
			}

		});

		casesStage.addActor(trois);

		TextButton quatre = new TextButton("", skin);
		quatre.setPosition(b + a / 2 - 15, a + c + d / 2 - 15);
		quatre.setSize(30, 30);
		quatre.setVisible(true);
		quatre.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (tab[z] == 4) {
					card4 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/2.jpg")));
					z++;
					if (z == 5) {
						initialisation();
						z = 0;
						Hacker.gsm.setScreens(STATE.PLAY);
						finishcases();
					}
				} else {
					initialisation();
	            	z=0;
	            	Hacker.gsm.setScreens(STATE.Anim);
	            	Gdx.input.setInputProcessor(AnimationCases.animStage);
				}
			}

		});

		casesStage.addActor(quatre);

		TextButton cinq = new TextButton("", skin);
		cinq.setPosition(a + b + a / 2 - 15, a + c + d / 2 - 15);
		cinq.setSize(30, 30);
		cinq.setVisible(true);
		cinq.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (tab[z] == 5) {
					card5 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/2.jpg")));
					z++;
					if (z == 5) {
						initialisation();
						z = 0;
						Hacker.gsm.setScreens(STATE.PLAY);
						finishcases();
					}
				} else {
					initialisation();
	            	z=0;
	            	Hacker.gsm.setScreens(STATE.Anim);
	            	Gdx.input.setInputProcessor(AnimationCases.animStage);
				}
			}

		});

		casesStage.addActor(cinq);

		TextButton six = new TextButton("", skin);
		six.setPosition(a * 2 + b + a / 2 - 15, a + c + d / 2 - 15);
		six.setSize(30, 30);
		six.setVisible(true);
		six.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (tab[z] == 6) {
					card6 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/2.jpg")));
					z++;
					if (z == 5) {
						initialisation();
						z = 0;
						Hacker.gsm.setScreens(STATE.PLAY);
						finishcases();
					}
				} else {
					initialisation();
	            	z=0;
	            	Hacker.gsm.setScreens(STATE.Anim);
	            	Gdx.input.setInputProcessor(AnimationCases.animStage);
				}
			}

		});

		casesStage.addActor(six);

		TextButton sept = new TextButton("", skin);
		sept.setPosition(b + a / 2 - 15, a * 2 + c + d / 2 - 15);
		sept.setSize(30, 30);
		sept.setVisible(true);
		sept.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (tab[z] == 7) {
					card7 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/2.jpg")));
					z++;
					if (z == 5) {
						initialisation();
						z = 0;
						Hacker.gsm.setScreens(STATE.PLAY);
						finishcases();
					}
				} else {
					initialisation();
	            	z=0;
	            	Hacker.gsm.setScreens(STATE.Anim);
	            	Gdx.input.setInputProcessor(AnimationCases.animStage);
				}
			}

		});

		casesStage.addActor(sept);

		TextButton huit = new TextButton("", skin);
		huit.setPosition(a + b + a / 2 - 15, a * 2 + c + d / 2 - 15);
		huit.setSize(30, 30);
		huit.setVisible(true);
		huit.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (tab[z] == 8) {
					card8 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/2.jpg")));
					z++;
					if (z == 5) {
						initialisation();
						z = 0;
						Hacker.gsm.setScreens(STATE.PLAY);
						finishcases();
					}
				} else {
					initialisation();
	            	z=0;
	            	Hacker.gsm.setScreens(STATE.Anim);
	            	Gdx.input.setInputProcessor(AnimationCases.animStage);
				}
			}

		});

		casesStage.addActor(huit);

		TextButton neuf = new TextButton("", skin);
		neuf.setPosition(a * 2 + b + a / 2 - 15, a * 2 + c + d / 2 - 15);
		neuf.setSize(30, 30);
		neuf.setVisible(true);
		neuf.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (tab[z] == 9) {
					card9 = new TextureRegion(new Texture(Gdx.files.internal("assets/Cases/2.jpg")));
					z++;
					if (z == 5) {
						initialisation();
						z = 0;
						Hacker.gsm.setScreens(STATE.PLAY);
						finishcases();
					}
				} else {
					initialisation();
	            	z=0;
	            	Hacker.gsm.setScreens(STATE.Anim);
	            	Gdx.input.setInputProcessor(AnimationCases.animStage);
				}
			}

		});

		casesStage.addActor(neuf);

	}

	@Override
	public boolean keyDown(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub

	}

}
