package screen;

import Controller.DialogueController;
import Controller.PlayerController;
import Dialogue.Dialogue;
import Dialogue.DialogueBox;
import Dialogue.DialogueNode;
import Dialogue.OptionBox;
import Transition.FadeInTransition;
import Transition.FadeOutTransition;
import Utile.Action;
import Utile.AnimationSet;
//import Utile.HealthBar;

import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import main.Hacker;
import main.Settings;
import model.*;
import screen.ScreenManager.STATE;

public class GameScreen extends AbstractScreen {
	private InputMultiplexer multiplexer;
    //public static Actor player;
	public static Player player;
    public static PlayerController controller;
    private TileMap map;
    private Camera camera;      // on cr�e un objet cam�ra
    private SpriteBatch batch;
   
    
    private Texture bar1;
    private Texture bar2;
    private Texture bar3;
    private Texture bar4;
    private Texture bar5;
    private Texture bar6;
    private Texture argent;
    private BitmapFont font = new BitmapFont();
    
    private ScreenManager state;
    private Image pepe;
    public Music music;
    public static Integer valArgent = 1000;
    private float  test=0f;
    private float rate=1f;
    private int uiScale=2;
    private Stage uiStage;
    private Table root;
    public DialogueBox dialogueBox;
    private OptionBox optionBox;
    public static Viewport gameViewPort;
    private Dialogue dialogue;
    private static DialogueController dialogueController;
    private Texture healthBar;
    
    private ArrayList<Entity> entities;
	private Dialogue hehe;
    
    public GameScreen(Hacker app) {
        super(app);
        gameViewPort = new ScreenViewport();
        bar1 = new Texture("assets/Bar1.png");
        bar2 = new Texture("assets/Bar2.png");
        bar3 = new Texture("assets/Bar3.png");
        bar4 = new Texture("assets/Bar4.png");
        bar5 = new Texture("assets/Bar5.png");
        bar6 = new Texture("assets/Bar6.png");
        healthBar = bar1;
        argent = new Texture("assets/money.png");
        
        entities = new ArrayList<Entity>();
       
        
        batch = new SpriteBatch();
        TextureAtlas atlas = app.getAssetManager().get("assets/graphics_packed/textures.atlas",TextureAtlas.class);
        AnimationSet animations = new AnimationSet(
        		new Animation (0.3f/2f,atlas.findRegions("brendan_walk_north"),PlayMode.LOOP_PINGPONG),
        		new Animation (0.3f/2f,atlas.findRegions("brendan_walk_south"),PlayMode.LOOP_PINGPONG),
        		new Animation (0.3f/2f,atlas.findRegions("brendan_walk_east"),PlayMode.LOOP_PINGPONG),
        		new Animation (0.3f/2f,atlas.findRegions("brendan_walk_west"),PlayMode.LOOP_PINGPONG),
        		atlas.findRegion( "brendan_stand_north"),
        		atlas.findRegion( "brendan_stand_south"),
        		atlas.findRegion( "brendan_stand_east"),
        		atlas.findRegion( "brendan_stand_west")

        		);
        		
        		
    	pepe = new Image(new Texture("assets/png.jpg"));
    	initUI();	
    	//map= new TileMap(10,10, "Outside_A5.png", "Outside_B.png");
    	multiplexer = new InputMultiplexer();

      

        map= new TileMap(10,10, "Dawn_Ctiy_Tileset.png", "Dawn_Ctiy_Tileset.png", this);
        player = new Player(map,0,0,animations, 0, 0);
        entities.add(player);

        map.beginTranser(player);
        //player.addToMap();
         dialogueController = new DialogueController(dialogueBox,optionBox);
        controller=new PlayerController(player);
        multiplexer.addProcessor(0,dialogueController);
        multiplexer.addProcessor(1,controller);
        
        dialogue=new Dialogue();
        DialogueNode node1= new DialogueNode("Hey hello there!",0);
        DialogueNode node2= new DialogueNode("Do you want to visit the city or play a game?",1);
        DialogueNode node3= new DialogueNode("Aaaww! Great ! <3",2);
        DialogueNode node4= new DialogueNode("Great ! Have fun",3);
        //node1.makeLinear(node2.getID());
        node2.addChoice("Visit city", 2);
        node2.addChoice("Taquin", 3);
        
        dialogue.addNode(node1);
        dialogue.addNode(node2);
        dialogue.addNode(node3);
        dialogue.addNode(node4);
        dialogueController.startDialogue(dialogue);

        
      

        music = Gdx.audio.newMusic(Gdx.files.internal("assets/musics/game-theme.mp3"));
    	 music.setLooping(true);
    	 music.setVolume(0.01f);

    	 hehe=new Dialogue();
         
         DialogueNode nodeA= new DialogueNode("Bienvenu dans la station de metro !",0);
         DialogueNode nodeB= new DialogueNode("Ou voulez vous allez ?",1);
         DialogueNode nodeC= new DialogueNode("Bon voyage !",2);
         nodeA.makeLinear(nodeB.getID());
         nodeB.addChoice("Kanagawa", 2);
         nodeB.addChoice("Osaka", 2);
         nodeB.addChoice("Centre Ville", 2);
         nodeB.addChoice("Jardin Zen", 2);
         
         hehe.addNode(nodeA);
         hehe.addNode(nodeB);
         hehe.addNode(nodeC);
        
       

        
        camera= new Camera();
        
        
        //entities.add(new Pnj(map, animations.getStanding(Direction.SOUTH), 15, 10, (float)0.0, (float)0.0, false, "msg(yo! ca va?)\nmsg(moi ca va bien!)"));
        //entities.add(new Pnj(map, animations.getStanding(Direction.SOUTH), 19, 10, (float)0.0, (float)0.0, false, "msg(Tiens! enfant des Jurons!)\nmsg(Alors? y a-t-il de l'avancement dans ta quete?)"));
        
        //addEntitiesToMap();
        
       

    }
    
    public void addEntity(Entity ent) {
    	entities.add(ent);
    }
    
    public void addEntitiesToMap() {
    	for(Entity ent: entities) {
    		ent.addToMap();
    	}
    }
	
	public void delEntities() {
        entities = new ArrayList<Entity>();
		entities.add(player);
	}

    public static Player getPlayer() {
		return player;
	}
    public static DialogueController getDialogueController() {
    	return dialogueController;
    }
    
    public static void setPlayerPosition(int posX, int posY) {
    	float delta = 0;
		player.setX(posX);
		player.setWorldX(posX);
		player.setY(posY);
		player.setWorldY(posY);
		player.update(delta);
    }

	public static void setPlayer(int posX, int posY, boolean delete) {
		float delta = 0;
		//GameScreen.player = player;
		if(delete)
			player.delFromMap();
		player.setX(posX);
		player.setWorldX(posX);
		player.setY(posY);
		player.setWorldY(posY);
		player.update(delta);
		player.addToMap();
	}

	public static void setPlayer(int posX, int posY) {
		setPlayer(posX, posY, false);
	}

	@Override
    public void show() {
        Gdx.input.setInputProcessor(multiplexer);

    }
	
	public void updateEntities(float delta) {
		//System.out.println("updating entities");
		for(Entity ent: entities) {
			ent.update(delta);
			//System.out.println("one entity updated");
		}
	}

    @Override
    public void render(float delta) {
    	super.render(delta);
        

    	dialogueController.update(delta);
        controller.update(delta);
    	//player.update(delta);
    	updateEntities(delta);
    	heatlhBar();
        if (Camera.isCameraState()) {camera.update(player.getWorldX()+0.5f,player.getWorldY()+0.5f+2);}
        uiStage.act(delta);
        gameViewPort.setScreenSize(1280, 720);
        gameViewPort.apply();
        batch.begin();
    float worldStartX=Gdx.graphics.getWidth()/2-camera.getCameraX()*Settings.SCALED_TILE_SIZE;
    float worldStartY=Gdx.graphics.getHeight()/2-camera.getCameraY()*Settings.SCALED_TILE_SIZE;

    for (int x=0; x< map.getWidth() ;x++){//draws the map textures that are under the player
        for (int y=0; y< map.getHeight();y++ ){
            Tile tile=map.getTile(x,  y);
            if(tile!=null) {
            	tile.drawUnder(batch, worldStartX+x*Settings.SCALED_TILE_SIZE, worldStartY+y*Settings.SCALED_TILE_SIZE, Settings.SCALED_TILE_SIZE, Settings.SCALED_TILE_SIZE);
            }
        }
    }

    for (int x=0; x< map.getWidth() ;x++){//draws the entities
        //for (int y=0; y< map.getHeight();y++ ){
        for (int y=map.getHeight()-1; y>=0 ;y-- ){//on inverse comme �a les entit�s plus basses (� l'�cran) passent par dessus
            Tile tile=map.getTile(x,  y);
            if(tile!=null) {
            	//tile.drawEntities(batch, worldStartX+player.getWorldX()* Settings.SCALED_TILE_SIZE, worldStartY+player.getWorldY()*Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE);
            	tile.drawEntities(batch, worldStartX, worldStartY,Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE);
            }
        }
    }
    //batch.draw(player.getSprite(), worldStartX+player.getWorldX()* Settings.SCALED_TILE_SIZE, worldStartY+player.getWorldY()*Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE*1.5f);
    
    for (int x=0; x< map.getWidth() ;x++){//draws the map textures that are over the player
        for (int y=0; y< map.getHeight();y++ ){
            Tile tile=map.getTile(x,  y);
            if(tile!=null)
            	tile.drawOver(batch, worldStartX+x*Settings.SCALED_TILE_SIZE, worldStartY+y*Settings.SCALED_TILE_SIZE, Settings.SCALED_TILE_SIZE, Settings.SCALED_TILE_SIZE);
        }
    }

    font.draw(batch, "Niveau : "+controller.getLvl(), 720, 670);
    batch.draw(healthBar,590,640,100,50);
    
    font.draw(batch, valArgent.toString(), 470, 670);
    batch.draw(argent, 390,640,50,50);
    

    batch.end();
    uiStage.draw();
    if(dialogueController.isDialogueShowing()) {
        pepe.setVisible(true);

    }else {
    pepe.setVisible(false);
    }
    
    music.play();
    }

    @Override
    public void resize(int width, int height) {
    	batch.getProjectionMatrix().setToOrtho2D(0,0,width,height);
    	
    	gameViewPort.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void update(float delta) {
    	 if (Gdx.input.isKeyJustPressed(Keys.B)) {
    		 dialogueController.startDialogue(hehe);
    		 
    		 
    	 }
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
   }
  
    private void initUI() {
		 uiStage = new Stage(new ScreenViewport());
		uiStage.getViewport().update(Gdx.graphics.getWidth()/uiScale, Gdx.graphics.getHeight()/uiScale, true);
		
		 root = new Table();
		root.setFillParent(true);
		uiStage.addActor(root);
		Skin skin = new Skin();
		
		TextureAtlas uiAtlas = app.getAssetManager().get("assets/graphics_packed/ui/uipack.atlas");
		
		NinePatch buttonSquareBlue = new NinePatch(uiAtlas.findRegion("dialoguebox"), 10, 10, 5, 5);
		skin.add("dialoguebox", buttonSquareBlue);
		
		NinePatch optionbox = new NinePatch(uiAtlas.findRegion("optionbox"),6, 6, 6, 6);
		skin.add("optionbox", optionbox);
		
		skin.add("arrow", uiAtlas.findRegion("arrow"), TextureRegion.class);
		skin.add("hpbar_side", uiAtlas.findRegion("hpbar_side"), TextureRegion.class);
		skin.add("hpbar_bar", uiAtlas.findRegion("hpbar_bar"), TextureRegion.class);
		skin.add("green", uiAtlas.findRegion("green"), TextureRegion.class);
		skin.add("yellow", uiAtlas.findRegion("yellow"), TextureRegion.class);
		skin.add("red", uiAtlas.findRegion("red"), TextureRegion.class);
		skin.add("background_hpbar", uiAtlas.findRegion("background_hpbar"), TextureRegion.class);
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("assets/font/pkmnrsi.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 12;
		parameter.color = new Color(96f/255f, 96f/255f, 96f/255f, 1f);
		parameter.shadowColor = new Color(208f/255f, 208f/255f, 200f/255f, 1f);
		parameter.shadowOffsetX = 1;
		parameter.shadowOffsetY = 1;
		parameter.characters = "!  \"  #  $  %  &  '  (  )  *  +  ,  -  .  /  0  1  2  3  4  5  6  7  8  9  :  ;  <  =  >  ?  @  A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  [  \\  ]  ^  _  `  a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z  {  |  }  ~  \u2190  \u2191  \u2192  \u2193  \u2640  \u2642";
		
		BitmapFont font = generator.generateFont(parameter); // font size 12 pixels
		generator.dispose(); // don't forget to dispose to avoid memory leaks!
		font.getData().setLineHeight(16f);
		skin.add("font", font);
		
		BitmapFont smallFont = app.getAssetManager().get("assets/font/small_letters_font.fnt", BitmapFont.class);
		skin.add("small_letters_font", smallFont);
		LabelStyle labelStyle = new LabelStyle();
		labelStyle.font = skin.getFont("font");
		skin.add("default", labelStyle);
		LabelStyle labelStyleSmall = new LabelStyle();
		labelStyleSmall.font = skin.getFont("small_letters_font");
		skin.add("smallLabel", labelStyleSmall);
		Table dialogueTable = new Table();
		
		dialogueTable.add(pepe).pad(10).height(50).width(50);
		optionBox = new OptionBox(skin);
		dialogueBox = new DialogueBox(skin);
		dialogueTable.add(dialogueBox);
		optionBox.setVisible(false);
		dialogueTable.add(optionBox).pad(10);
		root.add(dialogueTable).expand().bottom().left();
		

	}
    public void tp(int x, int y) {
  	  getApp().startTransition(
     			this,
     			this,
     			new FadeOutTransition(0.5f,Color.BLACK,getApp().getAssetManager()),
     			new FadeInTransition(0.5f,Color.BLACK,getApp().getAssetManager()),
     			new Action (){
     						@Override
					public void action() {
  							setPlayer(x, y);


					}
     			});   	


    }
 
    private void heatlhBar() {
    	switch (controller.getHealthBar()) {
    	case 0:
    	    healthBar= bar1;
    	    break;
    	  case 1:
      	    healthBar= bar2;
    	    break;
    	  case 2:
      	    healthBar= bar3;
    	    break;
    	  case 3:
      	    healthBar= bar4;
    	    break;
    	  case 4:
      	    healthBar= bar5;
    	    break;
    	  case 5:
      	    healthBar= bar6;
    	    break;
    	 
    	}
	
    }
}
