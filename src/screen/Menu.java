package screen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.audio.Music;

import main.Hacker;
import model.Camera;
import screen.ScreenManager.STATE;


public class Menu extends AbstractScreen {
	   public static Stage stage;
	    static Skin skin;
	    private ScreenManager  screenManager;
	    public Batch batch;
	    public TextureRegion backgroundTexture;
	    
	    
	    public Menu(final Hacker app) {
	    	
	        super(app);
	         backgroundTexture = new TextureRegion(new Texture("assets/main_screen/Hacker3.jpg"), 0, 0, 1280, 720);
	         batch = new SpriteBatch();
	         
	         Music music = Gdx.audio.newMusic(Gdx.files.internal("assets/musics/menu-theme.mp3"));
	         music.setVolume(0.01f);
	         music.setLooping(true);
	         music.play();

	        int buttonOffSet = 20;
	        stage = new Stage();
	        Gdx.input.setInputProcessor(stage);
	        createBasicSkin();
	        TextButton newGameButton = new TextButton("Start Game", skin);
	        newGameButton.setPosition(Gdx.graphics.getWidth() / 2 - Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() / 2 + (newGameButton.getHeight() + buttonOffSet));
	        newGameButton.addListener(new ClickListener() {


	            @Override
	            public void clicked(InputEvent event, float x, float y) {
	            	music.stop();
	            	getApp().setScreen(getApp().getGameScreen());

	                }

	        });
	            stage.addActor(newGameButton);
	            TextButton saveButton = new TextButton("Save Game ",skin);
	            saveButton.setPosition(Gdx.graphics.getWidth() / 2 - Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() / 2 );
	            saveButton.addListener(new ClickListener() {

	            public void clicked(InputEvent event, float x, float y) {
	                //super.clicked(event, x, y);
	            	save.Save.saveGame();
	            	 getApp().setScreen(getApp().getGameScreen());
	            }
	        });
	        stage.addActor(saveButton);
	        
	        TextButton loadButton = new TextButton("Load Game ",skin);
	        loadButton.setPosition(Gdx.graphics.getWidth() / 2 - Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() / 2 - (newGameButton.getHeight() + buttonOffSet));
	        loadButton.addListener(new ClickListener() {
	            @Override
	            public void clicked(InputEvent event, float x, float y) {
	                super.clicked(event, x, y);
	                save.Save.loadSave();
	                music.stop();
	              getApp().setScreen(getApp().getGameScreen());
	            }
	        });
	        stage.addActor(loadButton);
	        TextButton exitButton = new TextButton("Exit Game ",skin);
	        exitButton.setPosition(Gdx.graphics.getWidth() / 2 - Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() / 2 +-2* (newGameButton.getHeight() + buttonOffSet));
	        exitButton.addListener(new ClickListener() {
	            @Override
	            public void clicked(InputEvent event, float x, float y) {
	            	Gdx.app.exit();

	            }
	        });
	        /*TextButton ShopButton = new TextButton("Shop", skin);
	        ShopButton.setPosition(Gdx.graphics.getWidth() - Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 2+-2* (newGameButton.getHeight() + buttonOffSet));
	        ShopButton.addListener(new ClickListener() {


	            @Override
	            public void clicked(InputEvent event, float x, float y) {
	            	
	            	Hacker.gsm.setScreens(STATE.Shop);
	            	Gdx.input.setInputProcessor(ShopScreen.shopStage);
	            	boolean[] tab = screen.ShopScreen.getAchat();
	        		for(int z=0;z<tab.length;z++ ) {
	        			System.out.println(tab[z]);
	        		}
	                }

	        });*/
	        //stage.addActor(ShopButton);
	        stage.addActor(exitButton);

	        TextButton toggleFullScreen = new TextButton("Full Screen Game", skin);
	        toggleFullScreen.setPosition(Gdx.graphics.getWidth() - Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() - 100);
	        toggleFullScreen.addListener(new ClickListener() {


	            @Override
	            public void clicked(InputEvent event, float x, float y) {
	            	super.clicked(event, x, y);
	            	if(Gdx.graphics.isFullscreen() == false) {Gdx.graphics.setDisplayMode(1280, 720, true);} else Gdx.graphics.setDisplayMode(1280, 720, false);;
	            	System.out.println(Gdx.graphics.isFullscreen());
	                }

	        });
	            stage.addActor(toggleFullScreen);

	        }
	    public void createBasicSkin (){
	        BitmapFont font = new BitmapFont();
	        skin= new Skin();
	        skin.add("default",font);

	        Pixmap pixmap= new Pixmap((int) Gdx.graphics.getWidth()/4,(int)Gdx.graphics.getHeight()/10, Pixmap.Format.RGB888);
	        pixmap.setColor(Color.WHITE);
	        pixmap.fill();
	        skin.add("background", new Texture(pixmap));

	        TextButton.TextButtonStyle  textButtonStyle = new TextButton.TextButtonStyle();
	        textButtonStyle.up= skin.newDrawable("background",Color.BLACK);
	        textButtonStyle.down= skin.newDrawable("background",Color.DARK_GRAY);
	        //textButtonStyle.checked= skin.newDrawable("background",Color.DARK_GRAY);
	        textButtonStyle.over= skin.newDrawable("background",Color.LIGHT_GRAY);
	        textButtonStyle.font= skin.getFont("default");
	        skin.add("default",textButtonStyle);

	    }

	    @Override
	    public void hide() {

	    }

	    @Override
	    public void pause() {

	    }

	    @Override
	    public void update(float delta) {

	    }

	    @Override
	    public void render(float delta) {
	        super.render(delta);
	       
			batch.begin();
			batch.draw(backgroundTexture,0,0);
			batch.draw(backgroundTexture, 0, Gdx.graphics.getHeight());
			batch.end();
	        stage.draw();
	        stage.act();
	        


	    }

	    @Override
	    public void resize(int width, int height) {

	    }

	    @Override
	    public void resume() {

	    }

	    @Override
	    public void show() {

	    }

	    @Override
	    public void dispose() {

	    }

}
