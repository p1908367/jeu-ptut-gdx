package screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Controller.PlayerController;
import main.Hacker;

public class Quest extends AbstractScreen{
	public PlayerController controller;
	private Texture img;
	private Texture img2;

	private Sprite sprite;
	private Sprite sprites;
    public Batch batch;
    public Batch batchs;
    public Sprite test;
	public Quest(Hacker app) {
		super(app);
		img = new Texture("assets/quest.png");
	    img.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	    img2 = new Texture("assets/quest2.png");
	    img2.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	    
	    
	    sprite = new Sprite(img);
        batch = new SpriteBatch();
        sprites = new Sprite(img2);
	    sprites.flip(false, true);
	    sprites.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	    sprites = new Sprite(img);
	    

	    sprite.flip(false, true);
	    sprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	    test =sprite;
	}

	
	
	@Override
	public void render(float delta) {
        super.render(delta);
       // heatlhBar();
        batch.begin();
		batch.draw(test.getTexture(), 0, 0, sprite.getWidth(), sprite.getHeight());
		batch.end();
        
   }
	
	
	
	
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}
	private void heatlhBar() {
    	switch (controller.getHealthBar()) {
    	case 0:
    	    test= sprite;
    	    break;
    	  case 1:
      	    test= sprites;
    	    break;
    	  
    	}
	
    }
}
