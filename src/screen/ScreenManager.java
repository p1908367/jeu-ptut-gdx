package screen;

import java.util.HashMap;

import main.Hacker;

public class ScreenManager {
	    public final Hacker app ;
	    private HashMap<STATE,AbstractScreen> gameScreens;
	    public static STATE state;
	    private GameScreen gameScreen;
	    public static enum STATE{
	        Menu,
	        PLAY,
	        Taquin,
	        Shop,
	        Space,
	        Quest,
	        Cases,
	        Anim

	    }
	    public ScreenManager ( final Hacker app){
	        this.app=app;
	        initGameScreen();
	        setScreens(STATE.Menu);
	        
	    }
	public static STATE getState() {
			return state;
		}
	private void initGameScreen(){
	        this.gameScreens = new HashMap<STATE,AbstractScreen>();
	        this.gameScreens.put(STATE.Menu,app.getMenu());
	        this.gameScreens.put(STATE.PLAY,app.getGameScreen());
	        this.gameScreens.put(STATE.Taquin,app.getTaquin());
	        this.gameScreens.put(STATE.Shop,app.getShop());
	        this.gameScreens.put(STATE.Space,app.getSpaceScreen());
	        this.gameScreens.put(STATE.Quest,new Quest(app));
	        this.gameScreens.put(STATE.Cases,new CasesScreen(app));
	        this.gameScreens.put(STATE.Anim,new AnimationCases(app));
	}
	    public void setScreens(STATE nextScreen) {
	    	
	        app.setScreen(gameScreens.get(nextScreen));
	        state=nextScreen;

	    }
	    public void dispose(){
	        for(AbstractScreen screen : gameScreens.values()){
	            if (screen!=null){
	                screen.dispose();
	            }
	        }
	    }
	    public GameScreen getGameScreen() {
	    	return (GameScreen) this.gameScreens.get(STATE.PLAY);
	    }
	}


