package screen;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import main.Hacker;
import screen.ScreenManager.STATE;

public class ShopScreen extends AbstractScreen implements ApplicationListener, InputProcessor{
	


	public static Stage shopStage;
    Skin skin;
    private ScreenManager screenManager;
    public Batch batch;
    public TextureRegion backgroundTexture;
        
    private static String[] text = {"1.jpg","2.jpg","3.jpg","4.jpg"};
    private static boolean[] achat= new boolean[text.length];
    private TextureRegion texture = new TextureRegion(new Texture(Gdx.files.internal("assets/shop/"+text[0])));
    int i=0;
    
    
	public ShopScreen(final Hacker app) {
		super(app);
		backgroundTexture = new TextureRegion(new Texture("assets/shop/background.jpg"));
		shopStage = new Stage();
        batch = new SpriteBatch();
        createShopSkin();
        
        TextButton adroite = new TextButton(">", skin);
        adroite.setPosition(980,322);									
        adroite.setSize(50,50);
        adroite.setVisible(true);
        adroite.addListener(new ClickListener() {


            @Override
            public void clicked(InputEvent event, float x, float y) {
            	if(i<text.length-1) {
            	texture = new TextureRegion(new Texture(Gdx.files.internal("assets/shop/"+text[i+1])));
            	i=i+1;
            	}
            	else {
            	texture = new TextureRegion(new Texture(Gdx.files.internal("assets/shop/"+text[0])));
            	i=0;	
            	}
                }

        });
        
        shopStage.addActor(adroite);
        //
        TextButton agauche = new TextButton("<", skin);
        agauche.setPosition(300,322);                                           
        agauche.setSize(50,50);
        agauche.setVisible(true);
        agauche.addListener(new ClickListener() {


            @Override
            public void clicked(InputEvent event, float x, float y) {
            	if(i>0) {
            	texture = new TextureRegion(new Texture(Gdx.files.internal("assets/shop/"+text[i-1])));
            	i=i-1;
            	}
            	else {
            		texture = new TextureRegion(new Texture(Gdx.files.internal("assets/shop/"+text[text.length-1])));
                	i=text.length-1;
            	}
                }

        });
        shopStage.addActor(agauche);
        //
        TextButton retour = new TextButton("Retour", skin);
        retour.setPosition(20,20);
        retour.setSize(100,30);
        retour.setVisible(true);
        retour.addListener(new ClickListener() {


            @Override
            public void clicked(InputEvent event, float x, float y) {
            	
            	/*Hacker.gsm.setScreens(STATE.Menu);
            	Gdx.input.setInputProcessor(Menu.stage);*/
            	getApp().setScreen(getApp().getGameScreen());
                }

        });
        
        shopStage.addActor(retour);
		//
        TextButton buy = new TextButton("BUY", skin);   
        buy.setPosition(590,20);
        buy.setSize(100,30);
        buy.setVisible(true);
        buy.addListener(new ClickListener() {


            @Override
            public void clicked(InputEvent event, float x, float y) {
            		achat[i]=true;	
                }

        });
        
        shopStage.addActor(buy);
		
		
	}
	
	public static boolean[] getAchat() {
		return achat;
	}
	
	public void createShopSkin (){
        BitmapFont font = new BitmapFont();
        skin= new Skin();
        skin.add("default",font);

        Pixmap pixmap= new Pixmap((int) Gdx.graphics.getWidth()/4,(int)Gdx.graphics.getHeight()/10, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background", new Texture(pixmap));

        TextButton.TextButtonStyle  textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up= skin.newDrawable("background",Color.BLACK);
        textButtonStyle.down= skin.newDrawable("background",Color.DARK_GRAY);
        textButtonStyle.over= skin.newDrawable("background",Color.LIGHT_GRAY);
        textButtonStyle.font= skin.getFont("default");
        skin.add("default",textButtonStyle);

    }
	
	@Override
	public void render(float delta) {
        super.render(delta);
       
		batch.begin();
		shopStage.act();
		batch.draw(backgroundTexture,0,0,1280,720);
		batch.draw(texture,440,160,400,400);
		batch.end();
		shopStage.draw();
        
   }


	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		
	}
	

}
