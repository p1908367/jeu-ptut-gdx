package theHacker;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import health_level_money_bar.Health_Bar;
import main.Hacker;


public class theHacker {
	
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();	
	new LwjglApplication(new Hacker(), config);
		config.title="The Hacker";
		config.height=720;
		config.width =1280;
		config.addIcon("assets/game_icon.png", FileType.Internal);
		config.vSyncEnabled=true;
		config.resizable = false;
		
	}
	
}